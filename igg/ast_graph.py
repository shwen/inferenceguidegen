import unittest, sys
from dataclasses import dataclass

import ast as A
import networkx as Nx
import pyro.distributions as PD

from igg.common import *
from igg.expressions import *
import igg.directives

def get_call_keywords(call: A.Call, name: str):
    for kw in call.keywords:
        if kw.arg is not None and str(kw.arg) == name:
            return kw.value
    return None

class Scope:

    def __init__(self, control: Node, parent = None, context = None):
        assert parent or context
        assert isinstance(control, Node)
        assert isinstance(control.name, tuple)
        self.control = control
        if parent:
            self.context = parent.context
            self.parent = parent
        else:
            self.context = context
            self.parent = None

        self.flowCount = 0

        # Disctionary mapping variable tail names to variables
        self.v = dict()

    def get_node(self, name: str, ntype: NType):
        """
        Can only be used to create new nodes.
        """
        n = self.context.get_node(name, ntype, self.control)
        if ntype == NType.VARIABLE:
            if name in self.v:
                raise RuntimeError('Scope name collision')
            self.v[name] = n
        return n

    def add_control_dep(self, src):
        if src.tail_name in self.v:
            # The variable originated from this scope. End
            return
        if self.parent is None:
            raise RuntimeError(f"Undefined variable in scope: {str(src)}")
        if self.control.ntype in {NType.CONTROL_IF_THEN, NType.CONTROL_IF_ELSE}:
            # special case used to handle if's
            assert self.control.anchor
            self.context.add_dep(src, self.control.anchor)
        else:
            self.context.add_dep(src, self.control)
        self.parent.add_control_dep(src)

    def add_dep(self, src, dst):
        if isinstance(src, str):
            src = self.find_variable(src)
        self.context.add_dep(src, dst)
        self.add_control_dep(src)

    def find_variable(self, name):
        if name in self.v:
            return self.v[name]
        if self.parent:
            return self.parent.find_variable(name)

        raise RuntimeError(f"Undefined variable in scope: {name}")



    def get_if(self, expr: Expression = None):
        self.flowCount += 1

        node = self.get_node(f'_if{self.flowCount}', NType.CONTROL_IF)
        node.expr = expr
        nodeThen = self.context.get_node(f't', NType.CONTROL_IF_THEN, node)
        nodeElse = self.context.get_node(f'f', NType.CONTROL_IF_ELSE, node)

        nodeThen.anchor = node
        nodeElse.anchor = node

        scopeThen = Scope(control=nodeThen, parent=self)
        scopeElse = Scope(control=nodeElse, parent=self)

        return node, scopeThen, scopeElse
    def get_return(self, expr: A.expr = None):
        n = self.context.get_return_node(self.control)
        n.expr = expr
        return n
    def get_variable(self, name: str, expr: A.expr = None):
        n = self.get_node(name, NType.VARIABLE)
        n.expr = expr
        return n

    def shape_context(self, name):
        v = self.find_variable(name)
        assert v.expr, f"Variable with null expression is given: {name}"
        return v.expr.shape



class Context:

    def __init__(self, name, entry, args: list = []):
        self.name = name
        self.entry = entry
        self.siteCount = 0
        self.returnCount = 0
        self.varList = {str(self.entry): self.entry}
        self.depList = []
        self.args = args

    def create_scope(self):
        s = Scope(control=self.entry, context=self)
        s.v = {x.tail_name:x for x in self.args}
        return s

    # Used by scopes
    def get_node(self, name: str, ntype: NType, parent: Node, register:bool=True):
        """
        If register is false, don't register the dependency (useful for if's)
        """
        assert isinstance(name, str)
        n = Node(parent.name + (name,), ntype)
        if str(n) in self.varList:
            print(self.varList)
            raise RuntimeError(f"Variable name collision in context {str(n)}")
        self.varList[str(n)] = n
        if register:
            self.depList.append((str(parent), str(n)))
        return n
    def get_return_node(self, parent: Node):
        self.returnCount += 1
        name = f'_return{self.returnCount}'
        return self.get_node(name, NType.RETURN, parent)
    def add_dep(self, src, dst):
        if dst.anchor:
            dst = dst.anchor
        self.depList.append((str(src), str(dst)))



@dataclass
class Function:

    nodeEntry: Node
    """
    List of nodes
    """
    args: list
    extraArgs: list
    signature: A.expr

class AstGraph:


    def __init__(self):
        self.graph = Nx.DiGraph()
        self.nodes = dict()
        self.functions = dict()

        # Used to determine call cycles.
        self.callGraph = Nx.DiGraph()

        # Functions that are not processed stochastically
        self.auxiliaryFunctions = []

        self.directives = {}

    def process(self, module: A.Module, verbose: int = 0):
        """
        Process an entire module, which may contain one or several models.
        """
        def should_process(x):
            if not isinstance(x, A.FunctionDef):
                # only function definitions may be processed.
                return False
            return x.name.startswith('model')
        # List all the functions in this module
        functions = [x for x in module.body if should_process(x)]
        self.auxiliaryFunctions = [x for x in module.body
                                   if isinstance(x, A.FunctionDef) and not should_process(x)]
        contexts = [self.process_function_signature(f, verbose=verbose) for f in functions]

        for c,f in zip(contexts, functions):
            self.process_function(c, f)

    def process_function_signature(self, functionDef: A.FunctionDef, verbose: int = 0):
        funcName = str(functionDef.name)
        self.callGraph.add_node(funcName)
        # Entry variable also associated with the main function context
        nodeEntry = Node(name=(funcName,),
                        ntype=NType.ENTRY)
        arguments = [Node(name=(funcName, str(a.arg)),
                          ntype=NType.ARGUMENT)
                     for a in functionDef.args.args]
        # Attempt to infer the shape of arguments from default parameters
        # The defaults only contain the tail args
        for arg in arguments:
            arg.expr = Expression(None, EKind.OTHER, None, True)
        extraArgumentNodes = dict()
        for arg,expr in zip(reversed(arguments),reversed(functionDef.args.defaults)):
            shape, isConstant = expression_type_of(expr, lambda x:None)
            arg.expr = Expression(expr, EKind.OTHER, shape, isConstant)

            extraList = []

            if isinstance(shape, dict):
                for k,v in shape.items():
                    name = f"{arg.tail_name}/{k}"
                    ea = Node(name=(funcName, name), ntype=NType.ARGUMENT)
                    ea.expr = Expression(None, EKind.OTHER, v, isConstant)
                    extraList.append(ea)
            extraArgumentNodes[arg.tail_name] = extraList

        function = Function(
            nodeEntry=nodeEntry,
            args={str(a.arg) : var
                  for a,var in zip(functionDef.args.args, arguments)},
            extraArgs = {x.tail_name:x for v in extraArgumentNodes.values() for x in v},
            signature=functionDef.args,
        )
        if verbose >= 2:
            print(f"[AstGraph.process_function_signature] Detected function {str(functionDef.name)} with arguments {[v.tail_name for v in function.args.values()]}")
        self.functions[funcName] = function
        context = Context(funcName, nodeEntry, arguments
                          + [x for v in extraArgumentNodes.values() for x in v])
        self.add_node(nodeEntry)
        for a in arguments:
            self.add_node(a)
            self.add_dependency(nodeEntry, a)

            for extra in extraArgumentNodes[a.tail_name]:
                self.add_node(extra)
                self.add_dependency(a, extra)

        return context

    def process_function(self, context: Context, function: A.FunctionDef):
        scope = context.create_scope()
        for s in function.body:
            scope = self.process_statement(s, scope)
        for n in context.varList.values():
            assert isinstance(n, Node)
            self.add_node(n)
        for src, dst in context.depList:
            self.add_dependency(src, dst)

    def process_statement(self, s, scope: Scope):
        assert scope
        def unfold(s):
            unfolded, scope.context.siteCount = expression_unfold(s, scope.context.siteCount)
            return unfolded
        def exp_type_of(s):
            return expression_type_of(s, scope.shape_context)

        if isinstance(s, A.Expr):
            if isinstance(s.value, A.Constant) and isinstance(s.value.value, str):
                self.process_comment(s.value.value, scope)
            else:
                unfolded = unfold(s.value)
                self.process_expression_list(unfolded, scope)
        elif isinstance(s, A.Assign):
            # Adding multi-target support should be very easy
            assert len(s.targets) == 1, "Multi-target (not tuple) assignment is not yet supported"
            target = s.targets[0]
            if not isinstance(target, A.Name):
                raise RuntimeError(f"Assignment target must be a single variable {A.dump(s)}")

            targetName = str(target.id)
            unfolded = unfold(s.value)
            self.process_expression_list(unfolded, scope, scope.get_variable(targetName))

        elif isinstance(s, A.Return):
            unfolded = unfold(s.value)
            self.process_expression_list(unfolded, scope, scope.get_return())

        elif isinstance(s, A.If):
            nodeIf, scopeThen, scopeElse = scope.get_if()
            unfolded = unfold(s.test)
            self.process_expression_list(unfolded, scope, nodeIf)

            for t in s.body:
                scopeThen = self.process_statement(t, scopeThen)
            if s.orelse:
                for t in s.orelse:
                    scopeElse = self.process_statement(t, scopeElse)

        elif isinstance(s, A.Pass):
            pass
        elif isinstance(s, A.Break) or isinstance(s, A.Continue):
            raise RuntimeError(f"Cannot handle control flow statements")
        else:
            raise RuntimeError(f"Statement cannot be handled: {A.dump(s)}")

        return scope

    def process_expression_list(self, li: list, scope: Scope, nodeVar: Node = None):
        """
        li should be in the same format as the output of expression_unfold

        if nodeVar is supplied, it is taken to be the last node in the list
        """
        for i, (name, deps, kind, expr) in enumerate(li):
            if name is None:
                # None has to be the last element in the list
                assert i + 1 == len(li)

                # If no variable is supplied to collect this, the expression is
                # ignored. In the case for which this call is stochastic,
                # dependency cannot flow through this site anyways, but this
                # would create problems for SVI so the programmer should not
                # attempt to write something like this.

                # e.g.
                #
                #   a = sample(...)
                #   sample(...) # no observation
                #   stochCall(...) # Nothing happens to return value of the call
                if nodeVar is None:
                    continue
            if kind == EKind.CALL:
                # Add function to the call cycle.
                assert isinstance(expr, A.Call)
                assert isinstance(expr.func, A.Name)
                self.callGraph.add_edge(scope.context.name, str(expr.func.id))
            shape, isConstant = expression_type_of(expr, scope.shape_context)
            if i + 1 == len(li) and nodeVar:
                node = nodeVar
                node.expr = Expression(expr, kind, shape, isConstant)
                if node is not None and node.tail_name in self.directives:
                    node.directive = self.directives[node.tail_name]
                    if node.expr.shape is None:
                        node.expr.shape = node.directive.shapeHint
                    if node.directive.expressionKind is not None:
                        node.expr.kind = {
                            'DETERMINISTIC': EKind.DETERMINISTIC,
                            'STOCHASTIC': EKind.CALL,
                        }[node.directive.expressionKind]
                if kind == EKind.CALL and node.ntype == NType.RETURN:
                    sys.stderr.write(f"[AG.PEL] Warning: Return statement may not interact well with stochastic call {scope.context.name} -> {str(node)}\n")
            else:
                node = scope.get_variable(name, Expression(expr, kind, shape, isConstant))
            if kind == EKind.SAMPLE:
                # Compute the shapes of the arguments and store as metadata.
                node.expr.shapesArg = [expression_type_of(a, scope.shape_context)[0]
                                       for a in expr.args[1].args]
            for dName in deps:
                scope.add_dep(dName, node)

    def process_comment(self, x, scope):
        if not x.startswith("%") or not x.endswith("%"):
            # Not an annotation. Stop
            return
        x = x[1:-1]
        symbol, directive = igg.directives.Directive.parse(x)
        directive.embeddingName = f'_embed_{scope.context.name}_{symbol}'
        self.directives[symbol] = directive

        # See if directive symbol matches argument
        for a in scope.context.args:
            if a.tail_name == symbol:
                a.directive = directive



    def add_node(self, node: Node):
        s = str(node)
        assert s is not None
        if node.ntype == NType.UNKNOWN:
            raise RuntimeError(f"Warning: Node has unknown ntype: {str(node)}")
        self.graph.add_node(s, ntype=node.ntype)
        self.nodes[s] = node
    def add_dependency(self, src: Node, dst: Node):
        if isinstance(src, Node):
            self.graph.add_edge(str(src), str(dst))
        elif isinstance(src, list):
            for x in src:
                self.add_dependency(x, dst)
        elif isinstance(src, str):
            assert src in self.nodes
            self.graph.add_edge(src, str(dst))
        else:
            raise RuntimeError(f"Adding dependency of an unknown src type")

    def plot_graph(self, node_size=250):
        switch  = {
            NType.ENTRY: 'tab:red',
            NType.ARGUMENT: 'tab:gray',
            NType.VARIABLE: 'tab:green',
            NType.RETURN: 'tab:blue',

            NType.CONTROL_IF: 'tab:orange',
            NType.CONTROL_IF_THEN: 'tab:orange',
            NType.CONTROL_IF_ELSE: 'tab:orange',

            NType.UNKNOWN: 'white',
        }
        g = self.graph
        node_color = [switch[vt] for _,vt in g.nodes(data='ntype')]
        pos = Nx.spring_layout(g)
        Nx.draw_networkx_nodes(g, pos, node_color=node_color, node_size = node_size)
        Nx.draw_networkx_labels(g, pos)
        Nx.draw_networkx_edges(g, pos, edgelist=g.edges(), arrows=True)
    def plot_call_graph(self, node_size=250):
        g = self.callGraph
        def color_switch(name):
            if any(True for _ in g.predecessors(name)):
                return 'tab:green'
            else:
                return 'tab:red'
        node_color = [color_switch(name) for name in g.nodes()]
        pos = Nx.spring_layout(g)
        Nx.draw_networkx_nodes(g, pos, node_color=node_color, node_size = node_size)
        Nx.draw_networkx_labels(g, pos)
        Nx.draw_networkx_edges(g, pos, edgelist=g.edges(), arrows=True)

        for f,g in g.edges():
            print(f"Call: {f} -> {g}")



class TestAstGraph(unittest.TestCase):

    def test_context_scope(self):
        entry = Node(("main",), NType.ENTRY)
        context = Context("main", entry)

        scope = context.create_scope()
        v_x = scope.get_variable("x")
        v_y = scope.get_variable("y")
        c_if, scope1t, scope1f = scope.get_if()

        v_z1 = scope1t.get_variable("z1")
        scope1t.add_dep("x", v_z1)

        v_r1 = scope1f.get_return()
        scope1f.add_dep("y", v_r1)

        self.assertEqual([str(x) for x in context.varList.values()], [
            'main',
            'main.x',
            'main.y',
            'main._if1',
            'main._if1.t',
            'main._if1.f',
            'main._if1.t.z1',
            'main._if1.f._return1'
        ])
        self.assertEqual(context.depList, [
            (str(entry), str(v_x)),
            (str(entry), str(v_y)),
            (str(entry), str(c_if)),
            (str(c_if), str(scope1t.control)),
            (str(c_if), str(scope1f.control)),
            (str(scope1t.control), str(v_z1)),
            (str(v_x), str(v_z1)),
            (str(v_x), str(c_if)),
            (str(scope1f.control), str(v_r1)),
            (str(v_y), str(v_r1)),
            (str(v_y), str(c_if)),
        ])


if __name__ == '__main__':
    unittest.main()
