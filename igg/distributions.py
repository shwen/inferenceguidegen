import ast as A
from igg.expressions import *

def create_simple_distribution_handler(params):
    def handler(name, shapesArgs):
        target = f"_{name}_params"
        shapeOut = shapesArgs[0]
        shapeSize = math.prod(shapeOut) if shapeOut != () else 1

        def n_th_tensor_param(i):
            if shapeOut == ():
                return A.parse(f"{target}[{i}]", mode='eval')
            else:
                start = i * shapeSize
                end = (i + 1) * shapeSize
                shapeExpr = A.Tuple([A.Constant(m) for m in shapeOut], A.Load())
                return ast_call(('T', 'reshape'),
                                [
                                    A.parse(f"{target}[{start}:{end}]", mode='eval'),
                                    shapeExpr
                                ])

        args = [n_th_tensor_param(i) for i in range(len(params))]
        args = [ast_call(f, args=[a]) if f else a
                for a,(_,f) in zip(args, params)]
        return \
            A.Name(target, A.Store()), \
            args, \
            len(params) * shapeSize, \
            shapeSize
    return handler

def distribution_handler_categorical(name, shapesArgs):
    target = f"_{name}_params"
    pShape = shapesArgs[0]
    shapeSize = math.prod(pShape)
    shapeExpr = A.Tuple([A.Constant(m) for m in pShape], A.Load())
    args = [
        ast_call(F_T_SIGMOID, args=[
            ast_call(('T', 'reshape'),
                [
                    ast_name(target),
                    shapeExpr
                ])
        ])
    ]
    return \
        A.Name(target, A.Store()), \
        args, \
        shapeSize, \
        1

F_TNF_SOFTPLUS = ('TNF', 'softplus')
F_TN_SOFTMAX = ('TN', 'Softmax')
F_T_SIGMOID = ('T', 'sigmoid')

("""
Each distribution has a handler. The handler takes two parameters:
1. name, the name of the temporary variable used to store the result
2. shapesArgs: The shapes of individual arguments given to the distribution

returns:

1. The target of assignment statement
2. Arguments to the newly constructed distribution expression
   (e.g. ... inside PD.Normal(...))
3. The output tensor shape that would be required on {name}_params
""")
DISTRIBUTIONS = {
    'Normal': create_simple_distribution_handler([('mean', None), ('std', F_TNF_SOFTPLUS)]),
    'Poisson': create_simple_distribution_handler([('rate', F_TNF_SOFTPLUS)]),
    'Bernoulli': create_simple_distribution_handler([('prob', F_T_SIGMOID)]),
    'Categorical': distribution_handler_categorical,
}
