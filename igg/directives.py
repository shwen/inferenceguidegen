import re
import ast as A

class Directive:

    def __init__(self):
        self.shapeHint = None
        self.embedding = None
        self.embeddingShape = None
        self.embeddingName = None
        self.isPostReturn = False
        self.expressionKind = None

    @staticmethod
    def parse(s):

        regex = re.compile('\s+')
        symbol, s = regex.split(s.lstrip(), 1)
        print("Symbol: ",symbol)

        result = Directive()

        # Split directive by line
        lines = re.split(';', s)

        for line in lines:
            line = line.strip()
            if not line:
                continue
            # Split the next directive
            splitted = regex.split(line, 1)
            if len(splitted) == 1:
                command, = splitted
                if command == 'POSTRETURN':
                    result.isPostReturn = True
                continue

            command, content = splitted
            if command == 'SHAPE':
                result.shapeHint = A.literal_eval(content)
            elif command == 'EMBED':
                s1, s2 = regex.split(content, 1)
                result.embeddingShape = A.literal_eval(s1)
                result.embedding = A.parse(s2, mode='eval')
            elif command == 'EKIND':
                assert content in {'DETERMINISTIC', 'STOCHASTIC'}
                result.expressionKind = content
            else:
                raise RuntimeError(f"Invalid directive {command}")
        return symbol, result






