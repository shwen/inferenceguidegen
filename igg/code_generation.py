import collections, math, unittest

import ast as A
import numpy as N
import networkx as Nx

from igg.common import *
from igg.ast_graph import AstGraph
from igg.distributions import *

class GenerationContext:

    def __init__(self, name):
        self.name = name
        self.v = dict()
        # Always condition on the env
        self.env = []
        self.envTails = set()

    def add_variable(self, node):
        assert node.ntype.is_value
        assert node.tail_name not in self.v
        self.v[node.tail_name] = node
    def add_env(self, node):
        self.env.append(node)
        self.envTails.add(node.tail_name)

    def subcontext(self):
        sub = GenerationContext(self.name)
        sub.v = self.v.copy()
        sub.env = self.env.copy()
        sub.envTails = self.envTails.copy()
        return sub

    def exists(self, x):
        return x.tail_name in self.v \
            or x.tail_name in self.envTails


class CodeGenerator:

    def __init__(self, astGraph: AstGraph, use_gmade: bool, verbose: int,
                 n_hidden_layers: int, n_hidden_neurons: int, activation_fn: str):
        self.a = astGraph
        self.verbose = verbose
        self.use_gmade = use_gmade
        self.n_hidden_layers = n_hidden_layers
        self.n_hidden_neurons = n_hidden_neurons
        self.activation_fn = activation_fn

        # Caches all the neural networks by function
        self.nn = {}

        self.hiddenSizes = {}

        self.gmade_prefix = "gmade"
        self.gmade_index = 0

    def generate_tree(self):
        # Import the necessary modules
        module = A.parse("""
import torch as T
import torch.distributions as TD
import torch.distributions.constraints as TDC
import torch.nn as TN
import torch.nn.functional as TNF
import pyro as P
import pyro.infer as PI
import pyro.optim as PO
import pyro.distributions as PD
import pyro.nn as PN
from gmade import GMADE
        """, mode='exec')

        # Generate the stochastic functions
        envs = [
            self.create_function_environment(name, f)
            for name,f in self.a.functions.items()
        ]
        functions = [
            self.generate_function(name,f,context,args)
            for (name,f),(context,args) in zip(self.a.functions.items(), envs)
        ]

        nn_definitions = []
        for func,subdict in self.nn.items():
            for name,expr in subdict.items():
                if name.endswith("_gmade_") and type(expr) == dict:
                    gmade_init_info = expr
                    nn_definitions.append(A.Assign(targets=[A.Name(name + "input_dim_dict", expr_context=A.Store())],
                                     value=gmade_init_info["input_dim_dict"]))
                    nn_definitions.append(A.Assign(targets=[A.Name(name + "dependency_dict", expr_context=A.Store())],
                                     value=gmade_init_info["dependency_dict"]))
                    nn_definitions.append(A.Assign(targets=[A.Name(name + "var_dim_dict", expr_context=A.Store())],
                                     value=gmade_init_info["var_dim_dict"]))
                    nn_definitions.append(A.Assign(targets=[A.Name(name + "dist_type_dict", expr_context=A.Store())],
                                     value=gmade_init_info["dist_type_dict"]))
                    nn_definitions.append(A.Assign(targets=[A.Name(name, expr_context=A.Store())],
                                     value=gmade_init_info["gmade_expr"]))
                else:
                    nn_definitions.append(A.Assign(targets=[A.Name(name, expr_context=A.Store())],
                                     value=expr))

        module.body += nn_definitions

        #module.body += [A.Assign(targets=[A.Name(name, expr_context=A.Store())],
        #                         value=expr)
        #                for func,subdict in self.nn.items()
        #                for name,expr in subdict.items()]
        module.body += [A.Assign(
            targets=[A.Name(d.embeddingName, expr_context=A.Store())],
            value=d.embedding
        ) for d in self.a.directives.values() if d.embedding]
        module.body += functions
        module.body += self.a.auxiliaryFunctions
        return module
    def create_function_environment(self, funcName, func):
        context = GenerationContext(funcName)

        isCallee = any(True for _ in self.a.callGraph.predecessors(funcName))
        for arg in func.args.values():
            assert isinstance(arg, Node)
            context.add_variable(arg)
        for arg in func.extraArgs.values():
            assert isinstance(arg, Node)
            context.add_variable(arg)

        args = func.signature
        if isCallee:
            if self.verbose >= 1:
                print(f"[Code Generator] Is a callee")
            # Add a hidden state
            hiddenName = '_hidden'
            hiddenShape = (16,)
            self.hiddenSizes[funcName] = hiddenShape[0]
            hiddenNode = Node((funcName, hiddenName),
                              ntype = NType.ARGUMENT,
                              expr = Expression(None, EKind.OTHER, hiddenShape, False))
            context.add_env(hiddenNode)

            # Try to add other argument nodes fi they are marked with
            # isPostReturn
            for a in func.args.values():
                if a.directive and a.directive.isPostReturn:
                    context.add_env(a)

            self.a.nodes[str(hiddenNode)] = hiddenNode
            args.args = [A.arg(arg=hiddenName, annotation=None, type_comment=None)] \
                + args.args
        return context, args

    def generate_function(self, funcName, func, context, args):
        if self.verbose >= 1:
            print(f"[CodeGenerator] == Function: {funcName} ==")


        body = self.generate_block(func.nodeEntry, context=context)

        registers = [
            A.Expr(ast_call(('P', 'module'),
                   [A.Constant(name), ast_name(name)]))
            for name in self.nn[funcName].keys()
        ]

        return A.FunctionDef(
            name=funcName,
            args=args,
            body=registers + body,
            decorator_list=[],
            returns=None,
            type_comment=None
        )

    def _get_dep_id(self, ast_dep):
        if isinstance(ast_dep, A.Subscript):
            return ast_dep.value.id
        elif isinstance(ast_dep, A.Name):
            return ast_dep.id
        else:
            raise ValueError("dep type not supproted:", A.dump(ast_dep))
    
    def _subscript_to_str(self, subscript_expr):
        if isinstance(subscript_expr.slice.value, A.Constant):
            return f"{subscript_expr.value.id}['{subscript_expr.slice.value.value}']"
        else:
            raise ValueError("slice type not supproted:", A.dump(subscript_expr.slice.value))

    def generate_block(self, control, context):
        # Collect the subgraph under this control node
        subgraph = self.a.graph.subgraph([x for x in self.a.graph.successors(str(control))
                                          if self.a.nodes[x].ntype != NType.ARGUMENT])

        # Order the block
        ordered = block_reorder(subgraph, self.a.nodes)
        if self.verbose >= 1:
            print("[CodeGenerator.generate_block]", ordered)

        result = []
        result_stack = []
        self.gmades = []

        def reset_gmade_info():

            gmade_init_info = {
                "input_dim_dict" : {},
                "var_dim_dict" : {},
                "dependency_dict" : {},
                "dist_type_dict" : {}
            }
            gmade_input_dict = {}
            gmade_var_name_map = {}
            return gmade_init_info, gmade_input_dict, gmade_var_name_map
        
        def gmade_dist_mapping(distName, distSize):
            
            gmade_dist_name_mapping = {
                "Normal" : "norm",
                "Bernoulli" : "bern",
                "Categorical" : "cate"
            }
            res = gmade_dist_name_mapping[distName]
            if res == "cate":
                res = ("cate", distSize)
            return res
        
        # TODO we need more expression checks for this
        def arg_is_in_gmade(arg, gmade_var_set):
            if isinstance(arg, A.Name):
                return arg.id in gmade_var_set
            elif isinstance(arg, A.BinOp):
                return arg_is_in_gmade(arg.left, gmade_var_set) or \
                       arg_is_in_gmade(arg.right, gmade_var_set)
            elif isinstance(arg, A.Call):
                res = False
                for a in arg.args:
                    res = res or arg_is_in_gmade(a, gmade_var_set)
                return res
            elif isinstance(arg, A.List):
                res = False
                for a in arg.elts:
                    res = res or arg_is_in_gmade(a, gmade_var_set)
                return res
            else:
                return False

        
        gmade_init_info, gmade_input_dict, gmade_var_name_map = reset_gmade_info()

        num_rvs = 0
        for nodeName in ordered:
            num_rvs += self.a.nodes[nodeName].ntype == NType.VARIABLE and self.a.nodes[nodeName].expr.kind == EKind.SAMPLE
        use_gmade = False if num_rvs <= 0 else self.use_gmade

        returned = False

        # Now write each node to the result
        for nodeName in ordered:
            
            node = self.a.nodes[nodeName]
            print(nodeName, node.ntype)
            if node.ntype == NType.VARIABLE:
                print(str(node.expr))
                
                inScopeSet = {str(v) for v in context.v.values()}
                if node.expr.kind == EKind.SAMPLE:
                    trailEndSet = find_trails(self.a.graph, nodeName, inScopeSet, self.a.nodes,
                                              env=[str(e) for e in context.env])
                    deps = list(set().union(*[extract_dependents(self.a.graph, x, self.a.nodes)
                                                 for x in trailEndSet]))
                    deps = [self.a.nodes[d] for d in deps]

                    print(f"Generating dependency ({context.name}): {node.tail_name} from {trailEndSet}")
                    assert len(deps) > 0
                    print(f"Context: {[str(v) for v in context.v]}")
                    # Filter deps by variables visible in context
                    deps_raw = [ x for x in deps if context.exists(x)]
                    print(f"Deps: {[str(d) for d in deps_raw]}")
                    deps = [ x.embedded_expr for x in deps_raw]

                    _, distName = attribute_to_tuple(node.expr.expr.args[1].func)
                    print(f"[CC] Generating variable: {node} from dependencies {[A.dump(d) for d,_ in deps]}")
                    depsExpr, depsSize = shapes_merge_expression(deps)
                    distTarget, distArgs, distSize, node_size = DISTRIBUTIONS[distName](node.tail_name, node.expr.shapesArg)
                    if use_gmade:
                        gmade_init_info["var_dim_dict"][node.tail_name] = node_size
                        gmade_init_info["dependency_dict"][node.tail_name] = [ self._get_dep_id(dep[0]) for dep in deps ]
                        gmade_init_info["dist_type_dict"][node.tail_name] = gmade_dist_mapping(distName, distSize)
                        for dep in deps:
                            if self._get_dep_id(dep[0]) not in gmade_init_info["var_dim_dict"]:
                                gmade_init_info["input_dim_dict"][self._get_dep_id(dep[0])] = dep[1] if dep[1] != () else 1
                                gmade_input_dict[self._get_dep_id(dep[0])] = self._subscript_to_str(dep[0])
                        if not isinstance(node.expr.expr.args[0], str):
                            # the rv name is a formatted string
                            gmade_var_name_map[A.Constant(node.tail_name)] = node.expr.expr.args[0]
                    else:
                        # Generate a neural network to handle this
                        # Hyperparameters are used here.
                        nnExpr = nn_expression(depsSize, distSize, self.n_hidden_layers, max(depsSize, distSize, self.n_hidden_neurons), self.activation_fn)
                        nnName = f"_nn_{context.name}_{node.tail_name}"
                        self.add_nn(context.name, nnName, nnExpr)
                        result += [
                            ast_assign(distTarget, \
                                       ast_call(nnName, [depsExpr])),
                            ast_assign(ast_name(node.tail_name), \
                                       ast_sample(node.expr.expr.args[0], \
                                                  distName, \
                                                  distArgs))
                        ]
                elif node.expr.kind == EKind.CALL:
                    trailEndSet = find_trails(self.a.graph, nodeName, inScopeSet, self.a.nodes,
                                              env=[str(e) for e in context.env])
                    print(f"Generating dependency ({context.name}): {node.tail_name} from {trailEndSet}")
                    deps = list(set().union(*[extract_dependents(self.a.graph, x, self.a.nodes)
                                                 for x in trailEndSet]))
                    deps = [self.a.nodes[d] for d in deps]
                    assert len(deps) > 0
                    # Filter deps by variables visible in context
                    deps = [ x.embedded_expr for x in deps if context.exists(x)]
                    depsExpr, depsSize = shapes_merge_expression(deps)
                    calleeHiddenSize = self.hiddenSizes[str(node.expr.expr.func.id)]

                    # Hyperparameters
                    nnExpr = nn_expression(depsSize, calleeHiddenSize, self.n_hidden_layers, self.n_hidden_neurons, self.activation_fn)
                    nnName = f"_nn_{node.tail_name}"
                    self.add_nn(context.name, nnName, nnExpr)
                    expr = A.Call(node.expr.expr.func, \
                                  (ast_call(nnName, [depsExpr]),) + node.expr.expr.args,
                                  node.expr.expr.keywords
                                  )
                    if use_gmade and len(gmade_input_dict) > 0:
                        # if one of the arg is the output of gmade
                        if not arg_is_in_gmade(depsExpr, set(gmade_init_info['var_dim_dict'].keys())):
                            for arg in node.expr.expr.args:
                                if arg_is_in_gmade(arg, set(gmade_init_info['var_dim_dict'].keys())):

                                    result = self.gen_gmade(context.name, f"{self.gmade_prefix}_{self.gmade_index}",
                                                            gmade_init_info, gmade_input_dict, gmade_var_name_map, result,
                                                            result_stack)
                                    result_stack = []
                                    gmade_init_info, gmade_input_dict, gmade_var_name_map = reset_gmade_info()
                                    self.gmade_index += 1
                                    break
                        else:
                            result = self.gen_gmade(context.name, f"{self.gmade_prefix}_{self.gmade_index}",
                                                            gmade_init_info, gmade_input_dict, gmade_var_name_map, result,
                                                            result_stack)
                            result_stack = []
                            gmade_init_info, gmade_input_dict, gmade_var_name_map = reset_gmade_info()
                            self.gmade_index += 1
                    result += [
                        ast_assign(ast_name(node.tail_name),
                                   expr)
                    ]

                elif node.expr.kind == EKind.OBSERVE:
                    result.append(ast_assign(ast_name(node.tail_name),
                                             node.expr.through))
                else:
                    if use_gmade:
                        result_stack.append(ast_assign(ast_name(node.tail_name),
                                             node.expr.expr))
                    else:
                        result.append(ast_assign(ast_name(node.tail_name),
                                             node.expr.expr))

            elif node.ntype == NType.RETURN:
                assert node.expr.kind != EKind.SAMPLE
                if use_gmade and len(gmade_input_dict) > 0:
                    name = f"{self.gmade_prefix}_{self.gmade_index}"
                    result = self.gen_gmade(context.name, name, gmade_init_info, 
                                            gmade_input_dict, gmade_var_name_map, result,
                                            result_stack)
                    result_stack = []
                    returned = True
                    gmade_init_info, gmade_input_dict, gmade_var_name_map = reset_gmade_info()
                    self.gmade_index += 1

                result.append(A.Return(node.expr.through))

            elif node.ntype == NType.CONTROL_IF:
                node_f, node_t = list(self.a.graph.successors(nodeName))
                if self.a.nodes[node_f].ntype == NType.CONTROL_IF_THEN:
                    node_t, node_f = node_f, node_t
                context_f = context.subcontext()
                context_t = context.subcontext()

                block_f = self.generate_block(node_f, context_f)
                block_t = self.generate_block(node_t, context_t)

                if use_gmade and len(gmade_input_dict) > 0:
                    name = f"{self.gmade_prefix}_{self.gmade_index}"
                    result = self.gen_gmade(context.name, name, gmade_init_info, 
                                            gmade_input_dict, gmade_var_name_map, result,
                                            result_stack)
                    result_stack = []
                    returned = True
                    gmade_init_info, gmade_input_dict, gmade_var_name_map = reset_gmade_info()
                    self.gmade_index += 1


                result.append(A.If(node.expr.expr, block_t, block_f))

            # Tail processing

            if node.ntype == NType.VARIABLE:
                context.add_variable(node)

        if use_gmade and not returned and len(gmade_input_dict) > 0:
            name = f"{self.gmade_prefix}_{self.gmade_index}"
            result = self.gen_gmade(context.name,
                                    name,
                                    gmade_init_info,
                                    gmade_input_dict,
                                    gmade_var_name_map,
                                    result,
                                    result_stack)
            self.gmade_index += 1

        return result

    def gen_gmade(self, func, name, gmade_init_info, gmade_input_dict, gmade_var_name_map, result, result_stack):
        rvs = gmade_init_info['var_dim_dict'].keys()
        self.add_gmade(func, name, gmade_init_info)

        gmade_input_dict = A.Dict(keys=tuple(map(lambda x : A.Constant(x), tuple(gmade_input_dict.keys()))),
                                 values=tuple(map(lambda x : A.Name(id=x, ctx=A.Load()), tuple(gmade_input_dict.values()))))

        result.append(ast_assign(A.Name(id=name + "_input_dict", ctx=A.Store()), gmade_input_dict))
        result.append(ast_assign(A.Name(id=name + "_output_dict", ctx=A.Store()), ast_call(name + "_gmade_", [A.Name(id=name + "_input_dict", ctx=A.Load()), A.Dict(keys=(gmade_var_name_map.keys()), values=(gmade_var_name_map.values()))])))

        for rv in rvs:
            result.append(A.parse(f"{rv} = {name + '_output_dict'}['{rv}']"))

        result += result_stack

        return result

    def add_nn(self, func, name, expr):
        if self.verbose >= 2:
            print(f"Adding neural network {func}.{name}")
        if func not in self.nn:
            self.nn[func] = {}
        subdict = self.nn[func]
        assert name not in subdict
        subdict[name] = expr

    def add_gmade(self, func, name, gmade_init_info):

        gmade_init_info["var_dim_dict"] = A.Dict(keys=tuple(map(lambda x : A.Constant(x), tuple(gmade_init_info["var_dim_dict"].keys()))),
                                         values=tuple(map(lambda x : A.Constant(x), tuple(gmade_init_info["var_dim_dict"].values()))))
        gmade_init_info["input_dim_dict"] = A.Dict(keys=tuple(map(lambda x : A.Constant(x), tuple(gmade_init_info["input_dim_dict"].keys()))),
                                         values=tuple(map(lambda x : A.Constant(x), tuple(gmade_init_info["input_dim_dict"].values()))))
        gmade_init_info["dependency_dict"] = A.Dict(keys=tuple(map(lambda x : A.Constant(x), tuple(gmade_init_info["dependency_dict"].keys()))),
                                         values=tuple(map(lambda x : A.List(list(map(lambda x_i : A.Constant(x_i), x)), ctx=A.Load()), tuple(gmade_init_info["dependency_dict"].values()))))
        gmade_init_info["dist_type_dict"] = A.parse(str(gmade_init_info["dist_type_dict"]), mode='eval')
        gmade_init_info["gmade_expr"] = gmade_expression(name + "_gmade_input_dim_dict",
                                                         name + "_gmade_dependency_dict", 
                                                         name + "_gmade_var_dim_dict", 
                                                         name + "_gmade_dist_type_dict",
                                                         hidden_sizes = 4,
                                                         hidden_layers = 1)
        if func not in self.nn:
            self.nn[func] = {}
        subdict = self.nn[func]
        assert name not in subdict
        subdict[name + "_gmade_"] = gmade_init_info

## Auxiliary Functions ##

def extract_dependents(graph, name, nodeMap):
    """
    Given an observation node, extract all of the dependent variables
    """
    showDiag = True
    if nodeMap[name].expr.kind == EKind.OBSERVE:
        result = {x for x in graph.predecessors(name)
                if nodeMap[x].ntype.is_value}
        if showDiag:
            print(f"\t[ED] Expand obs '{name}' to {result} ")
        return result
    elif not is_numeric_shape(nodeMap[name].embedded_expr[1]):
        if showDiag:
            print(f"\t[ED] Reject '{name}' for having non-numeric shape")
        return set()
    else:
        if showDiag:
            print(f"\t[ED] Accept '{name}'")
        return {name}


def block_reorder(graph, nodeMap):
    resultGraph = Nx.DiGraph()

    def find_stochastic_descendants(start):
        visited = set()
        result = set()

        def dfs(n):
            if n in visited:
                return
            visited.add(n)
            node = nodeMap[n]
            if node.ntype == NType.ARGUMENT:
                return
            if node.is_sample_site and n != start:
                result.add(n)

            for m in graph.successors(n):
                dfs(m)
        dfs(start)
        return result

    sourceNodes = [node for node, indegree in graph.in_degree(graph.nodes())
                   if indegree == 0]
    assert len(sourceNodes) > 0, "No source nodes found which should not happen"
    # Default ordering follows the DFS edges
    dfsOrder = [v for u,v in Nx.dfs_edges(graph, source=sourceNodes[0])]
    dfsOrder = list(reversed(dfsOrder))
    def lexicographical_key(n):
        if n in dfsOrder:
            return dfsOrder.index(n)
        else:
            return -1

    hasReturn = []
    
    # Add nodes except for return
    for n in graph:
        if nodeMap[n].ntype == NType.RETURN:
            if hasReturn:
                raise RuntimeError("Double return within the same block")
            hasReturn = [n]
        else:
            resultGraph.add_node(n)
    # Add data dependency edges
    for u,v in graph.edges():
        nu = nodeMap[u]
        nv = nodeMap[v]
        # Check type integrity
        assert nu.ntype != NType.RETURN
        if nv.ntype == NType.RETURN:
            continue

        if nv.is_sample_site:
            # No edges entering sample sites
            continue
        else:
            resultGraph.add_edge(u, v)
    # Add inference dependency edges
    sampleSites = []
    for u in graph:
        if nodeMap[u].is_sample_site:
            sampleSites.append(u)
            stochDesc = find_stochastic_descendants(u)
            for v in stochDesc:
                resultGraph.add_edge(v, u)

    # Sink all calls to the bottom
    sinkAllCalls = True
    if sinkAllCalls:
        for u in graph:
            nu = nodeMap[u]
            if nu.ntype == NType.VARIABLE and nu.expr.kind == EKind.CALL:
                for v in sampleSites:
                    resultGraph.add_edge(v, u)
    return list(Nx.lexicographical_topological_sort(resultGraph, key=lexicographical_key)) + hasReturn

def find_trails(graph, start, inScopeSet, nodeMap, env=[], d=-1):
    if d == -1:
        forward = find_trails(graph, start, inScopeSet, nodeMap, env=env, d=0)
        
        return forward
        #backward = find_trails(graph, start, inScopeSet, nodeMap, env=env, d=1)
        #return forward.union(backward)

    visitedSet = set()
    trailEndSet = set()

    showDiag = True
    if showDiag:
        print(f"\t[FT] Finding trails from {start} from inScopeSet {inScopeSet}")

    def bfs(n, flip=0):
        if n in visitedSet:
            return
        visitedSet.add(n)

        isNumeric = nodeMap[n].ntype in { NType.ARGUMENT, NType.VARIABLE, NType.RETURN }
        isReturn = nodeMap[n].ntype == NType.RETURN
        isStochastic = isNumeric and nodeMap[n].expr.kind in { EKind.SAMPLE, EKind.OBSERVE, EKind.CALL }
        isObserve = isNumeric and nodeMap[n].expr.kind == EKind.OBSERVE
        isDownwards = d + flip == 1
        isArgument = nodeMap[n].ntype == NType.ARGUMENT

        # Probability trail cannot pass through control nodes upwards
        if not isNumeric and not isDownwards:
            return

        if showDiag:
            inScopeFlag = n in inScopeSet
            moveStr = 'v' if isDownwards else '^'
            nodeStr = f'[{n}]' if inScopeFlag else f' {n} '

        mustTurn = False
        if isObserve or (n in inScopeSet and isStochastic) \
           or isArgument or (n in inScopeSet):
            if showDiag:
                if isArgument:
                    argStr = ' ARG'
                elif isStochastic:
                    argStr = ' SAMPLE'
                elif n in inScopeSet:
                    argStr = ' DET'
                else:
                    argStr = ' '
                print(f"\t[FT] --{moveStr} {nodeStr} |END{argStr}")
            trailEndSet.add(n)

            # Probability trail terminates here, unless this is of the type
            # start -> n <- other, in which case the turn proceeds but cannot
            # go in the same direction.
            mustTurn = d == 1 and flip == 0
            if not mustTurn:
                return
        elif isReturn:
            assert not isObserve
            if showDiag:
                print(f"\t[FT] --{moveStr} {nodeStr} |RETURN END")
            mustTurn = True
            for e in env:
                trailEndSet.add(e)
        elif showDiag:
            print(f"\t[FT] --{moveStr} {nodeStr} --")


        # The turn of a v-shaped trail can only happen on stochastic nodes.

        pred, succ = graph.predecessors(n), graph.successors(n)
        if d == 1:
            pred, succ = succ, pred

        pred = list(pred)
        succ = list(succ)
        #if showDiag:
        #    print(f"\t[FT] {pred}, {succ}")
        if flip == 0 and (not mustTurn):
            for m in pred:
                bfs(m, flip = 0)
        if flip == 1 or isStochastic or isReturn:
            if showDiag and flip == 0:
                print(f"\t[FT] Flip at {n}")
            for m in succ:
                bfs(m, flip = 1)
    bfs(start)
    if showDiag:
        print(f"\t[FT] Found trail end {trailEndSet}")
    return trailEndSet



class TestCodeGeneration(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        def create_graph(li):
            graph = Nx.DiGraph()
            nodeMap = {name : Node(name, ntype,
                                   Expression(expr=None, kind=ekind, shape=None, isConstant=False))
                       for name, ntype, ekind, _ in li}
            for name, ntype, ekind, _ in li:
                graph.add_node(name)
            for name, _, _, deps in li:
                for dep in deps:
                    graph.add_edge(dep, name)
            return graph, nodeMap

        cls.g0, cls.n0 = create_graph([
            ('entry', NType.ENTRY, EKind.OTHER, set()),
            ('y11', NType.ARGUMENT, EKind.OTHER, {'entry'}),
            ('y12', NType.ARGUMENT, EKind.OTHER, {'entry'}),
            ('y21', NType.ARGUMENT, EKind.OTHER, {'entry'}),
            ('y22', NType.ARGUMENT, EKind.OTHER, {'entry'}),
            ('x', NType.VARIABLE, EKind.SAMPLE, {'entry'}),
            ('x1', NType.VARIABLE, EKind.SAMPLE, {'entry', 'x'}),
            ('x2', NType.VARIABLE, EKind.SAMPLE, {'entry', 'x'}),
            ('z11', NType.VARIABLE, EKind.OBSERVE, {'entry', 'x1', 'y11'}),
            ('z12', NType.VARIABLE, EKind.OBSERVE, {'entry', 'x1', 'y12'}),
            ('z21', NType.VARIABLE, EKind.OBSERVE, {'entry', 'x2', 'y21'}),
            ('z22', NType.VARIABLE, EKind.OBSERVE, {'entry', 'x2', 'y22'}),
        ])

        cls.g1, cls.n1 = create_graph([
            ('entry', NType.ENTRY, EKind.OTHER, set()),
            ('a', NType.ARGUMENT, EKind.OTHER, {'entry'}),
            ('x', NType.VARIABLE, EKind.SAMPLE, {'entry'}),
            ('u', NType.VARIABLE, EKind.OBSERVE, {'entry', 'x'}),
            ('_if1', NType.CONTROL_IF, EKind.OTHER, {'entry', 'x'}),
            ('y', NType.VARIABLE, EKind.SAMPLE, {'_if1', 'x'}),
            ('z', NType.RETURN, EKind.OBSERVE, {'_if1', 'y', 'a'})
        ])
        cls.g1b, cls.n1b = create_graph([
            ('entry', NType.ENTRY, EKind.OTHER, set()),
            ('a', NType.ARGUMENT, EKind.OTHER, {'entry'}),
            ('q', NType.VARIABLE, EKind.SAMPLE, {'entry'}),
            ('_if1', NType.CONTROL_IF, EKind.OTHER, {'entry', 'q'}),
            ('y', NType.VARIABLE, EKind.SAMPLE, {'_if1'}),
            ('z', NType.RETURN, EKind.OBSERVE, {'_if1', 'y', 'a'})
        ])
        cls.g2a, cls.n2a = create_graph([
            ('entry', NType.ENTRY, EKind.OTHER, set()),
            ('a', NType.ARGUMENT, EKind.OTHER, {'entry'}),
            ('q', NType.VARIABLE, EKind.SAMPLE, {'entry'}),
            ('_if1', NType.CONTROL_IF, EKind.OTHER, {'entry', 'q'}),
            ('_if1t', NType.CONTROL_IF_THEN, EKind.OTHER, {'_if1'}),
            ('_if1f', NType.CONTROL_IF_THEN, EKind.OTHER, {'_if1'}),
            ('y', NType.VARIABLE, EKind.SAMPLE, {'_if1t'}),
            ('r', NType.VARIABLE, EKind.CALL, {'_if1t'}),
            ('z', NType.RETURN, EKind.DETERMINISTIC, {'_if1t', 'y', 'r'})
        ])
        
        # This particualr cases tests trails coming from the other side
        cls.gLadder, cls.nLadder = create_graph([
            ('entry', NType.ENTRY, EKind.OTHER, set()),
            ('w', NType.VARIABLE, EKind.SAMPLE, {'entry'}),
            ('x1', NType.VARIABLE, EKind.SAMPLE, {'entry', 'w'}),
            ('x2', NType.VARIABLE, EKind.SAMPLE, {'entry', 'x1'}),
            ('x3', NType.VARIABLE, EKind.SAMPLE, {'entry', 'x2'}),
            ('x4', NType.VARIABLE, EKind.SAMPLE, {'entry', 'x3'}),
            ('y1', NType.VARIABLE, EKind.SAMPLE, {'entry', 'w'}),
            ('y2', NType.VARIABLE, EKind.SAMPLE, {'entry', 'y1'}),
            ('y3', NType.VARIABLE, EKind.SAMPLE, {'entry', 'y2'}),
            ('y4', NType.VARIABLE, EKind.SAMPLE, {'entry', 'y3'}),
            ('z', NType.VARIABLE, EKind.SAMPLE, {'entry', 'x4', 'y4'}),
            ('obs', NType.RETURN, EKind.OBSERVE, {'entry', 'z'})
        ])
        
    def test_extract_dependents(self):
        g,n = self.g0, self.n0
        self.assertEqual(extract_dependents(g, 'z11', n), {'x1', 'y11'})
        self.assertEqual(extract_dependents(g, 'z12', n), {'x1', 'y12'})
        self.assertEqual(extract_dependents(g, 'z21', n), {'x2', 'y21'})
        self.assertEqual(extract_dependents(g, 'z22', n), {'x2', 'y22'})
        g,n = self.g1, self.n1
        self.assertEqual(extract_dependents(g, 'z', n), {'y', 'a'})
    def test_find_trails(self):
        g,n = self.g0, self.n0
        args = {'y11','y12','y21','y22'}
        def f(start, inScopeSet, d):
            return find_trails(g, start, args.union(inScopeSet), n, d=d)
        # With x2 in scope, probability flow cuts off at x2
        self.assertEqual(f('x1', {'x2'}, 0), { 'x2', 'z12', 'z11' })
        self.assertEqual(f('x1', {'x2'}, 1), { 'y11', 'y12', 'z12', 'z11' })
        # With x2 out of scope, doesn't happen
        self.assertEqual(f('x1', set(), 0), { 'z21', 'z22', 'z11', 'z12' })
        self.assertEqual(f('x1', set(), 1), { 'z11', 'z12', 'y11', 'y12' })

        g,n = self.g1, self.n1
        args = {'a'}
        def f(start, inScopeSet, d):
            return find_trails(g, start, args.union(inScopeSet), n, d=d)
        self.assertEqual(f('x', {}, 1), { 'u', 'z', 'a' })
        self.assertEqual(f('x', {}, 0), { 'u', 'z' })
        self.assertEqual(f('y', { 'x' }, 1), { 'z', 'a', 'x' })
        g,n = self.g1b, self.n1b
        args = {'a'}
        # Probability trail goes through control nodes downwards - in this case
        # an observation out of scope is added to dependency set
        self.assertEqual(f('q', { '' }, 1), { 'z', 'a' })
        # but not upwards ...
        self.assertEqual(f('y', { 'q' }, 1), { 'z', 'a' })

        g,n = self.g2a, self.n2a
        args = {'a'}
        # Probability trail can reflect from return nodes
        self.assertEqual(f('r', { 'y' }, 1), { 'y' })

    def test_block_reorder(self):
        subg = self.g0.subgraph([x for x in self.g0.successors('entry')
                                 if self.n0[x].ntype != NType.ARGUMENT])
        # Multiple correct answers here, but the important thing
        # is that x is done after x1 and x2. the positions of z's don't matter.
        self.assertEqual(block_reorder(subg, self.n0),
                         ['x2', 'z22', 'z21', 'x1', 'x', 'z12', 'z11'])
        subg = self.g1.subgraph([x for x in self.g1.successors('entry')
                                 if self.n1[x].ntype != NType.ARGUMENT])
        # Multiple correct answers here
        self.assertEqual(block_reorder(subg, self.n1),
                         ['x', '_if1', 'u'])
        subg = self.g1.subgraph([x for x in self.g1.successors('_if1')
                                 if self.n1[x].ntype != NType.ARGUMENT])
        self.assertEqual(block_reorder(subg, self.n1),
                         ['y', 'z'])
    
    def test_ladder_trails(self):
        """
        We are interested in particular ordering cases on the ladder
        """
        g, n = self.gLadder, self.nLadder
        
        def f(start, inScopeSet, d=-1):
            return find_trails(g, start, {'z', 'obs'}.union(inScopeSet), n, d=d)
    
        self.assertEqual(f('y4', {'x4'}), { 'z', 'x4' })
        self.assertEqual(f('y1', {'y4', 'y3', 'y2', 'x4', 'x3', 'x2', 'x1'}), { 'y2', 'x1' })
        self.assertEqual(f('x4', {'y4', 'y3', 'y2', 'y1'}), { 'y1', 'z' })
        
        self.assertEqual(block_reorder(self.gLadder, self.nLadder),
                         ['entry', 'z', 'y4', 'y3', 'y2', 'y1', 'x4', 'x3', 'x2', 'x1', 'w', 'obs'])




if __name__ == '__main__':
    unittest.main()
