import enum
from dataclasses import dataclass
from igg.expressions import *

class NType(enum.Enum):

    ENTRY = 0

    ARGUMENT = 11
    VARIABLE = 12
    RETURN = 13

    CONTROL_IF = 20
    CONTROL_IF_THEN = 21
    CONTROL_IF_ELSE = 22

    UNKNOWN = 99

    @property
    def is_control(self):
        return self in { NType.ENTRY,
                         NType.CONTROL_IF,
                         NType.CONTROL_IF_THEN,
                         NType.CONTROL_IF_ELSE }
    @property
    def is_value(self):
        return self in { NType.ARGUMENT, NType.VARIABLE }

    @property
    def tag(self):
        return (({
            NType.ENTRY: '!',
            NType.VARIABLE: '$',
            NType.RETURN: '#',
            NType.CONTROL_IF: '_if',
            NType.CONTROL_IF_THEN: '_ifthen',
            NType.CONTROL_IF_ELSE: '_ifelse',
        }))

class Node:

    def __init__(self,
                 name: tuple,
                 ntype: NType = NType.UNKNOWN,
                 expr: Expression=None):
        self.name = name
        self.ntype = ntype
        self.expr = expr
        self.anchor = None # used for if's
        self.directive = None

    @staticmethod
    def join_tuple(t):
        return ".".join(t)
    def __str__(self):
        return self.join_tuple(self.name)
    @property
    def tail_name(self):
        return self.name[-1]

    @property
    def is_sample_site(self):
        return self.ntype == NType.VARIABLE and self.expr.kind == EKind.SAMPLE

    @property
    def embedded_expr(self):
        """
        Return the embedded expression and shape. If there's no embedding then
        just return the ast name.
        """
        if '/' in self.tail_name:
            e = ast_chained_dict(self.tail_name.split('/'))
        else:
            e = ast_name(self.tail_name)

        if self.directive and self.directive.embedding:
            e = ast_call((self.directive.embeddingName,), [e])
            shape = self.directive.embeddingShape
            assert shape
            return e, shape
        else:
            shape = self.expr.shape
            return e, shape

