import enum, math, unittest
from dataclasses import dataclass

import ast as A

def formatted_string_to_name(e):
    if isinstance(e, A.Constant):
        return e.value
    def f(x):
        if isinstance(x, A.FormattedValue):
            return x.value.id
        elif isinstance(x, A.Constant):
            return x.value
        else:
            assert False, f"Unsupported JoinedStr: {A.dump(x)}"
    #print(str(e))
    return "_".join(f(x) for x in e.values)
def attribute_to_tuple(x: A.Attribute):
    """
    Convert a name attribute to a tuple. e.g.

    abc -> abc
    a.bc -> (a, bc)
    """
    if isinstance(x, A.Name):
        return (str(x.id),)
    elif isinstance(x, A.Attribute):

        return attribute_to_tuple(x.value) + (str(x.attr),)
    else:
        raise RuntimeError(f"Taking attribute of something other than a name")
def get_keyword(x: A.Call, key: str):
    for keyword in x.keywords:
        if key == keyword.arg:
            return keyword.value
    return None
def add_keyword(kli: list, key: str, expr: A.expr):
    return [kw for kw in kli if kw.arg != key] + [A.keyword(key, expr)]
def extract_constant(e: A.Constant):
    if not isinstance(e, A.Constant):
        raise RuntimeError(f"Not a constant: {A.dump(e)}")
    return e.value

FUNC_SAMPLE = ('P', 'sample')
FUNC_TENSOR = ('T', 'tensor')
FUNC_ZEROS = ('T', 'zeros')
FUNC_ONES = ('T', 'ones')

def shapes_merge(*shapes):
    if shapes.count(shapes[0]) != len(shapes):
        raise RuntimeError(f"Cannot merge unequal shapes {shapes[0]}")
    return shapes[0]


def tensor_shape_merge(shape1, shape2):
    if shape1 == () or shape1 is None:
        return shape2
    elif shape2 == () or shape2 is None:
        return shape1
    elif shape1 == shape2:
        return shape1
    else:
        raise RuntimeError(f"Incompatible shape: {shape1}, {shape2}")
def is_keyed_dictionary(e: A.Subscript):
    return isinstance(e.slice, A.Index) \
        and isinstance(e.slice.value, A.Constant) \
        and isinstance(e.slice.value.value, str) \
        and isinstance(e.value, A.Name)

DISTRIBUTION_SHAPE_ESTIMATE = {
    # Just the first arg
    'Normal': lambda e,c:expression_type_of(e.args[0],c)[0],
    'Poisson': lambda e,c:expression_type_of(e.args[0],c)[0],
    'Bernoulli': lambda e,c:expression_type_of(e.args[0],c)[0],
    # Tuple slicing
    'Categorical': lambda e,c:expression_type_of(e.args[0],c)[0][:-1],
}

def is_numeric_shape(shape):
    return shape is not None \
        and not isinstance(shape, str) \
        and not isinstance(shape, dict)

def expression_type_of(s, shapeContext):
    """
    Determine the shape and constantality of an expression. Err on the side of
    caution.
    """
    assert callable(shapeContext)

    def rec(s):
        return expression_type_of(s, shapeContext)

    if isinstance(s, A.BoolOp):
        shapes, isConstants = zip(*[rec(t) for t in s.values])

        return shapes_merge(*shapes), all(isConstants)
    elif isinstance(s, A.NamedExpr):
        return rec(s.value)
    elif isinstance(s, A.BinOp):
        shapeL, isConstantL = rec(s.left)
        shapeR, isConstantR = rec(s.right)
        if (isinstance(shapeL, tuple) or shapeL is None) \
        	and (isinstance(shapeR, tuple) or shapeR is None):
            shape = tensor_shape_merge(shapeL, shapeR)
        else:
            shape = shapeL
        return shape, isConstantL and isConstantR
    elif isinstance(s, A.UnaryOp):
        return rec(s.operand)
    elif isinstance(s, A.Lambda):
        return "lambda", False
    elif isinstance(s, A.IfExp):
        _, isConstantT = rec(s.test)
        shapeB, isConstantB = rec(s.body)
        shapeE, isConstantE = rec(s.orelse)
        return shapes_merge(shapeB, shapeE), \
            isConstantT and isConstantB and isConstantE
    elif isinstance(s, A.Dict):
        keys = [extract_constant(k) for k in s.keys]
        values, isConstantV = zip(*[rec(v) for v in s.values])
        return { k:v for k,v in zip(keys, values) }, all(isConstantV)
    elif isinstance(s, A.Set):
        isConstant = [rec(t).isConstant for t in s.elts]
        return "set", isConstant
    elif isinstance(s, A.ListComp):
        return "list", False
    elif isinstance(s, A.SetComp):
        return "set", False
    elif isinstance(s, A.DictComp):
        return "dict", False
    elif isinstance(s, A.GeneratorExp):
        return "generator", False
    elif isinstance(s, A.Compare):
        isConstants = [rec(x)[1] for x in [s.left] + list(s.comparators)]
        return "bool", all(isConstants)
    elif isinstance(s, A.Call):
        if isinstance(s.func, A.Attribute):
            funcPath = attribute_to_tuple(s.func)
            if funcPath in { FUNC_ZEROS, FUNC_ONES }:
                shape = s.args[0]
                if not isinstance(shape, A.Tuple):
                    raise RuntimeError(f"Shape given is not a tuple: {A.dump(shape)}")

                if not all(isinstance(e, A.Constant) for e in shape.elts):
                    raise RuntimeError(f"A variadic shape is not supported: {A.dump(shape)}")

                shape = tuple(e.value for e in shape.elts)

                return shape, True
            elif funcPath == FUNC_TENSOR:

                if isinstance(s.args[0], A.List) or isinstance(s.args[0], A.Tuple):
                    shapes, eltsConstants = zip(*[rec(x) for x in s.args[0].elts])
                    if any(s != () for s in shapes):
                        return None, all(eltsConstants)
                    return (len(s.args[0].elts),), all(eltsConstants)
                else:
                    assert len(s.args) == 1
                    _, isConstant = rec(s.args[0])
                    return (), isConstant
            elif funcPath == FUNC_SAMPLE:
                obsExpr = get_keyword(s, "obs")
                if obsExpr:
                    return rec(obsExpr)
                else:
                    _, distName = attribute_to_tuple(s.args[1].func)

                    shape = DISTRIBUTION_SHAPE_ESTIMATE[distName](s.args[1], shapeContext)
                    return shape, False

        return None, False
    elif isinstance(s, A.FormattedValue):
        return "str", False
    elif isinstance(s, A.JoinedStr):
        return "str", False
    elif isinstance(s, A.Constant):
        if isinstance(s.value, str):
            return "str", True
        elif isinstance(s.value, bool):
            return bool, True
        elif isinstance(s.value, float):
            return (), True
        elif isinstance(s.value, int):
            return (), True
        else:
            return None, True
    elif isinstance(s, A.Attribute):
        raise RuntimeError(f"Cannot handle attribute {A.dump(s)}")
    elif isinstance(s, A.Subscript):
        if is_keyed_dictionary(s):
            # Special case when shape is given in dict form
            # Indexing by string
            key = s.slice.value.value
            dictShape = shapeContext(s.value.id)
            assert isinstance(dictShape, dict)
            assert key in dictShape
            assert not isinstance(dictShape[key], str)
            return dictShape[key], False

        shapeI, _ = rec(s.value)
        # There is a way to determine if this is constant but it isn't useful.
        if isinstance(shapeI, tuple):
            return (), False
        elif isinstance(shapeI, str):
            return "str", False
        elif isinstance(shapeI, bool) or isinstance(shapeI, float) \
             or isinstance(shapeI, int):
            raise RuntimeError(f"Cannot take slice of boolean {A.dump(s)}")
        else:
            return None, False
    elif isinstance(s, A.Starred):
        raise RuntimeError(f"Cannot handle expression {A.dump(s)}")
    elif isinstance(s, A.Name):
        shape = shapeContext(s.id)
        return shape, False
    elif isinstance(s, A.Slice):
        raise RuntimeError(f"Slice should not appear here {A.dump(s)}")
    else:
        raise RuntimeError(f"Cannot handle expression {A.dump(s)}")


class EKind(enum.Enum):

    SAMPLE = 0
    CALL = 1
    OBSERVE = 2
    DETERMINISTIC = 3
    OTHER = 4

def expression_unfold(s, nextId=0):
    """
    Unfold call sites and random samples. e.g.

    P.sample("x", distX) + 1 + model_random(w) + P.sample("z", distZ, obsZ)

    becomes

    [
		('x', {...}, SAMPLE, P.sample(...)), # ... contain dependencies for x in distX
    	(_site1, {w}, CALL, model_random(w)),
    	('z', {...}, OBSERVE, P.sample(...)),
    	(None, {x, _site1, z}, DETERMINISTIC, x+_site1+z), # None represents the return value
    ]
    """
    def rec(s):
        nonlocal nextId
        result, nextId = expression_unfold(s, nextId)
        return result
    def extract(s):
        nonlocal nextId
        if s is None:
            return [], set(), None
        li = rec(s)
        lastName, lastDep, lastKind, lastExpr = li[-1]
        if lastName is None and lastKind == EKind.DETERMINISTIC:
            # Result is a None expression, remove it.
            return li[:-1], lastDep, lastExpr
        elif lastName is None and lastKind == EKind.CALL:
            # Generate a 'site' variable
            siteName = f'_site{nextId}'
            nextId += 1
            return li[:-1] + [(siteName, lastDep, lastKind,lastExpr)], \
                {siteName}, A.Name(siteName, A.Load())
        else:
            return li, {lastName}, A.Name(lastName, A.Load())

    if isinstance(s, A.BoolOp):
        precs, deps, exprs = zip(*[extract(t) for t in values])
        expr = A.BoolOp(s.op, exprs)
        return [p for prec in precs for p in prec] + \
            [
                (None, set().union(*deps), EKind.DETERMINISTIC, expr)
            ], nextId
    elif isinstance(s, A.NamedExpr):
        li, deps, expr = extract(s.value)
        return [
            (None, deps, EKind.DETERMINISTIC, A.NamedExpr(s.target, expr)),
             ], nextId
    elif isinstance(s, A.BinOp):
        precsL, depsL, exprL = extract(s.left)
        precsR, depsR, exprR = extract(s.right)
        return precsL + precsR + [
            (None, depsL.union(depsR), EKind.DETERMINISTIC, A.BinOp(exprL, s.op, exprR))
        ], nextId
    elif isinstance(s, A.UnaryOp):
        precs, deps, expr = extract(s.operand)
        return precs + [
            (None, deps, EKind.DETERMINISTIC, A.UnaryOp(s.op, expr))
        ], nextId
    elif isinstance(s, A.Lambda):
        raise RuntimeError(f"Lambda expressions are not allows here: {A.dump(s)}")
    elif isinstance(s, A.IfExp):
        precsC, depsC, exprC = extract(s.test)
        precsL, depsL, exprL = extract(s.body)
        precsR, depsR, exprR = extract(s.orelse)
        return precsC + precsL + precsR + [
            (None, depsC.union(depsL, depsR), EKind.DETERMINISTIC, A.IfExp(s.test, s.body, s.orelse))
        ], nextId
    elif isinstance(s, A.Dict):
        raise RuntimeError(f"Not supported: {A.dump(s)}")
    elif isinstance(s, A.Set):
        raise RuntimeError(f"Not supported: {A.dump(s)}")
    elif isinstance(s, A.ListComp):
        raise RuntimeError(f"Not supported: {A.dump(s)}")
    elif isinstance(s, A.SetComp):
        raise RuntimeError(f"Not supported: {A.dump(s)}")
    elif isinstance(s, A.DictComp):
        raise RuntimeError(f"Not supported: {A.dump(s)}")
    elif isinstance(s, A.GeneratorExp):
        raise RuntimeError(f"Not supported: {A.dump(s)}")
    elif isinstance(s, A.Compare):
        precs, deps, exprs = zip(*[extract(t) for t in [s.left] + list(s.comparators)])
        expr = A.Compare(exprs[0], s.ops, exprs[1:])
        return [p for prec in precs for p in prec] + \
            [
                (None, set().union(*deps), EKind.DETERMINISTIC, expr)
            ], nextId
    elif isinstance(s, A.Call):
        # Cases: P.sample, P.sample (observe), stochastic call
        if isinstance(s.func, A.Attribute) and \
           attribute_to_tuple(s.func) == FUNC_SAMPLE:
            # sample function
            obsExpr = get_keyword(s, "obs")
            kind = EKind.OBSERVE if obsExpr else EKind.SAMPLE

            precsD, depsD, exprD = extract(s.args[1])
            if obsExpr:
                precsO, depsO, exprO = extract(obsExpr)
                precsD += precsO
                depsD = depsD.union(depsO)
                expr = A.Call(s.func, [s.args[0], exprD], add_keyword(s.keywords, 'obs', exprO))
            else:
                expr = A.Call(s.func, [s.args[0], exprD], s.keywords)
            return precsD + [
                (formatted_string_to_name(s.args[0]),
                 depsD,
                 kind,
                 expr),
            ], nextId
        elif isinstance(s.func, A.Name) and s.func.id.startswith('model_'):
            print(f"[Unfold] Callee: {s.func.id}")
            # call function
            precs, deps, exprs = zip(*[extract(t) for t in s.args])
            expr = A.Call(s.func, args=exprs, keywords=s.keywords)
            return [p for prec in precs for p in prec] + [(
                (None, set().union(*deps), EKind.CALL, expr)
            )], nextId
        else:
            # some other function
            if len(s.args) == 0:
                # Sometimes this happens (e.g. hailfinder) where a function is just constant
                expr = A.Call(s.func, args=[], keywords=s.keywords)
                return [(
                    (None, set(), EKind.DETERMINISTIC, expr)
                )], nextId
            
            print(f"[Unfold] Callee': {A.dump(s)}")
            precs, deps, exprs = zip(*[extract(t) for t in s.args])
            expr = A.Call(s.func, args=exprs, keywords=s.keywords)

            return [p for prec in precs for p in prec] + [(
                (None, set().union(*deps), EKind.DETERMINISTIC, expr)
            )], nextId
    elif isinstance(s, A.Constant):
        return [
            (None, set(), EKind.DETERMINISTIC, s)
        ], nextId
    elif isinstance(s, A.Name):
        name = s.id
        return [
            (None, {name}, EKind.DETERMINISTIC, s)
        ], nextId
    elif isinstance(s, A.List):
        precs, deps, exprs = zip(*[extract(t) for t in s.elts])
        expr = A.List(exprs, s.ctx)
        return [p for prec in precs for p in prec] + \
            [
                (None, set().union(*deps), EKind.DETERMINISTIC, expr)
            ], nextId
    elif isinstance(s, A.Subscript):
        if is_keyed_dictionary(s):
            name = s.value.id
            key = s.slice.value.value
            nodeName = f"{name}/{key}"
            return [
                (None, {nodeName}, EKind.DETERMINISTIC, s)
            ], nextId
        precV, depsV, exprV = extract(s.value)
        precL, depsL, exprL = extract(s.slice.lower)
        precU, depsU, exprU = extract(s.slice.upper)
        precS, depsS, exprS = extract(s.slice.step)
        return precV + precL + precU + precS + [
            (None, depsV.union(depsL,depsU,depsS),EKind.DETERMINISTIC,
             A.Subscript(exprV, A.Slice(exprL,exprU,exprS), s.ctx))
        ], nextId
    else:
        raise RuntimeError(f"Unsupported expression: {A.dump(s)}")




@dataclass
class Expression:

    expr: A.expr
    kind: EKind
    shape: tuple
    isConstant: bool

    @property
    def through(self):
        # Pass through the observed value
        if self.kind == EKind.OBSERVE:
            return get_keyword(self.expr, "obs")
        else:
            return self.expr
        
    def __str__(self):
        return f"{self.kind} {A.dump(self.expr)}"


### Expression creation ###

def shapes_merge_expression(li):
    """
    Create an expression which merges nodes into one and return the shape of
    the result.

    li: List of (expr, shape)
    """
    if len(li) == 0:
        raise RuntimeError("A list of size 0 cannot be merged.")
    for e,_ in li:
        assert isinstance(e, A.Name) or isinstance(e, A.Call) or is_keyed_dictionary(e)
    if all(shape == () for _, shape in li):
        # Special simplification case for all scalar variables
        expr = ast_call(('T', 'tensor'), [ast_list([x for x,_ in li])])
        return expr, len(li)
    if any(not is_numeric_shape(shape) for _, shape in li):
        errorStr = '\n'.join([
            str(shape) + " : " + A.dump(x)
            for x,shape in li
            if not is_numeric_shape(shape)
        ])
        raise RuntimeError(f"Merge list contains one or more unmergeable shapes {errorStr}")
    def single(ns):
        x, shape = ns
        if shape == ():
            return ast_call(('T', 'tensor'), [ast_list([x])])
        elif len(shape) == 1:
            return x
        else:
            return ast_call(('T', 'flatten'), [x])
    def size(shape):
        if shape == ():
            return 1
        else:
            return math.prod(shape)

    if len(li) == 1:
        # Singleton case
        totalSize = size(li[0][1])
        return single(li[0]), totalSize

    elements = [single(x) for x in li]
    totalSize = sum(size(shape) for _,shape in li)

    return ast_call(('T', 'cat'), [ast_list(elements)]), totalSize

def nn_expression(inSize, outSize, nHiddenLayers, nHiddenNeurons, activation_fn):
    activation_function = None
    if activation_fn.lower() == "relu":
        activation_function = "TN.ReLU"
    elif activation_fn.lower() == "sigmoid":
        activation_function = "TN.Sigmoid"
    elif activation_fn.lower() == "tanh":
        activation_function = "TN.Tanh"
    else:
        raise ValueError(f"does not support activation_fn={activation_fn}")

    expr = f"""PN.PyroModule[TN.Sequential](
    PN.PyroModule[TN.Linear]({inSize}, {nHiddenNeurons}),
    PN.PyroModule[{activation_function}](),
"""
    for _ in range(nHiddenLayers):
        expr += f"""
    PN.PyroModule[TN.Linear]({nHiddenNeurons}, {nHiddenNeurons}),
    PN.PyroModule[{activation_function}](),
"""
    expr += f"""
    PN.PyroModule[TN.Linear]({nHiddenNeurons}, {outSize})
)"""
    return A.parse(expr, mode='eval')

def gmade_expression(name_input_dim_dict, name_dependency_dict, name_var_dim_dict, name_dist_type_dict, hidden_sizes, hidden_layers):
    expr = f"""GMADE({name_input_dim_dict}, {name_dependency_dict}, {name_var_dim_dict}, {name_dist_type_dict}, hidden_sizes={hidden_sizes}, hidden_layers={hidden_layers})"""

    return A.parse(expr, mode='eval')


def tuple_targets(*names):
    return A.Tuple([A.Name(n, expr_context=A.Store()) for n in names],
                   expr_context=A.Store())

def ast_name(x):
    return A.Name(x, ctx=A.Load())
def ast_tuple_to_attr(t):
    if isinstance(t, str):
        return ast_name(t)
    if len(t) == 1:
        return ast_name(t[0])
    else:
        return A.Attribute(ast_tuple_to_attr(t[:-1]), attr=t[-1], ctx=A.Load())
def ast_assign(left, right):
    return A.Assign([left], right)
def ast_call(name: tuple, args: list, keywords: list = []):
    assert isinstance(args, list) or isinstance(args, tuple), f"Argument is not a list: {args}"
    assert isinstance(keywords, list)
    func = ast_tuple_to_attr(name) \
        if isinstance(name, tuple) or isinstance(name, str) \
           else name
    return A.Call(func=func, args=list(args), keywords=keywords)
def ast_sample(name, distributionName, args):
    astDistrib = ast_call(('PD', distributionName), args)
    if isinstance(name, str):
        name = A.Constant(value=name)
    return ast_call(('P', 'sample'), args=[name, astDistrib])
def ast_list(li: list):
    return A.List(elts=li, ctx=A.Load())
def ast_shape_to_tuple(shape: tuple):
    return A.Tuple([A.Constant(x) for x in shape], ctx=A.Load())
def ast_chained_dict(li: list):
    assert len(li) > 0
    if len(li) == 1:
        return ast_name(li[0])

    front = ast_chained_dict(li[:-1])
    back = li[-1]
    return A.Subscript(value=front,
                       slice=A.Index(A.Constant(back, ctx=A.Load())),
                       ctx=A.Load())


class TestExpressions(unittest.TestCase):

    def test_tensor_shape_merge(self):
        SCALAR = ()
        self.assertEqual(tensor_shape_merge( SCALAR, (3,4,5) ), (3,4,5))
        self.assertEqual(tensor_shape_merge( (1,2), SCALAR ), (1,2))
        self.assertEqual(tensor_shape_merge( SCALAR, SCALAR ), SCALAR )
        self.assertEqual(tensor_shape_merge( (4,5), None ), (4,5))
        self.assertEqual(tensor_shape_merge(None, None), None)
    def test_expression_type(self):
        context = {
            'cond': 'bool',
            'env1': (),
            'env2': (1,2,3),
            'env2b': (1,2,3),
        }
        def test_expr(s, shape, isConstant):
            self.assertEqual(expression_type_of(A.parse(s, mode='eval').body,
                                                lambda x:context[x]), \
                             (shape, isConstant))
        test_expr("1+2", (), True)
        test_expr("env1 * env2", (1,2,3), False)
        test_expr("env2[0,1,2]", (), False)
        test_expr("T.zeros((3,2,1))", (3,2,1), True)
        test_expr("T.ones((5,))", (5,), True)
        test_expr("'string'", 'str', True)
        test_expr("env2b + 1 if cond else env2", (1,2,3), False)
        test_expr("T.zeros((3,)) if 1 + 2 == 3 else T.ones((3,))", (3,), True)
        test_expr("P.sample('z', PD.Normal(1.0,0.1))", (), False)
        test_expr("P.sample('z', PD.Normal(T.tensor([-1.0,1.0]), obs=T.tensor([0.1,0.1])))", (2,), False)
        test_expr("P.sample('z', ignored, obs=env2)", (1,2,3), False)
        test_expr("T.tensor([1,2,3,4,5])", (5,), True)
        test_expr("{'a': T.zeros((3,2)), 'b': 2}", {'a': (3,2), 'b': ()}, True)

    def test_expression_unfold(self):
        import astor

        def case(s, expect, nextId):
            result, resultNextId = expression_unfold(A.parse(s, mode='eval').body)
            self.assertEqual([(a,b,c,astor.to_source(d).rstrip('\n')) for a,b,c,d in result], expect)
            self.assertEqual(resultNextId, nextId)

        case("P.sample('x', PD.Normal(1, 2)) + 1", [
            ('x', set(), EKind.SAMPLE, "P.sample('x', PD.Normal(1, 2))"),
            (None, {'x'}, EKind.DETERMINISTIC, "(x + 1)"),
        ], 0)
        case("P.sample('z', PD.Normal(model_f(a, b), 2), obs=obsZ)", [
            ('_site0', {'a','b'}, EKind.CALL, 'model_f(a, b)'),
            ('z', {'_site0', 'obsZ'}, EKind.OBSERVE, "P.sample('z', PD.Normal(_site0, 2), obs=obsZ)"),
        ], 1)
        exprStr = "a + b * c + d + deterministic(a, b, c, d)"
        case(exprStr, [
            (None, {'a','b','c','d'}, EKind.DETERMINISTIC, f"({exprStr})")
        ], 0)
        exprStr = "P.sample(f'z{i}', PD.Normal(1, 2))"
        case(exprStr, [
            ('z_i', set(), EKind.SAMPLE, exprStr)
        ], 0)

    def test_shapes_merge_expression(self):
        import astor

        def case(nameExprs, expectStr, expectTotalShape):
            nameExprs = [(ast_name(x), s) for x,s in nameExprs]
            expr, totalShape = shapes_merge_expression(nameExprs)
            self.assertEqual(astor.to_source(expr).rstrip('\n'), expectStr)
            self.assertEqual(totalShape, expectTotalShape)

        case([('s1', ())], "T.tensor([s1])", 1)
        case([('x1', (2,3))], "T.flatten(x1)", 6)
        case([
            ('x1', (2,3)), ('s1', ())
        ], "T.cat([T.flatten(x1), T.tensor([s1])])", 7)


if __name__ == '__main__':
    unittest.main()





