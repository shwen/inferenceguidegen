import torch as T
import torch.distributions as TD
import torch.distributions.constraints as TDC
import torch.nn as TN
import torch.nn.functional as TNF
import pyro as P
import pyro.infer as PI
import pyro.optim as PO
import pyro.distributions as PD
import pyro.nn as PN
import igg
from gmade import GMADE
_nn_model_sub_a = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](16,
    16), PN.PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](16, 16), PN.
    PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](16, 16), PN.
    PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](16, 2))
_nn_model_sub_yn = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](16,
    16), PN.PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](16, 16), PN.
    PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](16, 16), PN.
    PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](16, 2))
_nn__site0 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](17, 5),
    PN.PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](5, 5), PN.
    PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](5, 5), PN.PyroModule
    [TN.Sigmoid](), PN.PyroModule[TN.Linear](5, 5), PN.PyroModule[TN.
    Sigmoid](), PN.PyroModule[TN.Linear](5, 5), PN.PyroModule[TN.Sigmoid](),
    PN.PyroModule[TN.Linear](5, 5), PN.PyroModule[TN.Sigmoid](), PN.
    PyroModule[TN.Linear](5, 5), PN.PyroModule[TN.Sigmoid](), PN.PyroModule
    [TN.Linear](5, 5), PN.PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear
    ](5, 5), PN.PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](5, 16))
_nn_model_n = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 5),
    PN.PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](5, 5), PN.
    PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](5, 5), PN.PyroModule
    [TN.Sigmoid](), PN.PyroModule[TN.Linear](5, 5))
_nn_z = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 5), PN.
    PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](5, 5), PN.PyroModule
    [TN.Sigmoid](), PN.PyroModule[TN.Linear](5, 5), PN.PyroModule[TN.
    Sigmoid](), PN.PyroModule[TN.Linear](5, 5), PN.PyroModule[TN.Sigmoid](),
    PN.PyroModule[TN.Linear](5, 5), PN.PyroModule[TN.Sigmoid](), PN.
    PyroModule[TN.Linear](5, 5), PN.PyroModule[TN.Sigmoid](), PN.PyroModule
    [TN.Linear](5, 5), PN.PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear
    ](5, 5), PN.PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](5, 5),
    PN.PyroModule[TN.Sigmoid](), PN.PyroModule[TN.Linear](5, 16))


def model_sub(_hidden, n=0, __base_var_name__=''):
    _a_params = _nn_model_sub_a(_hidden)
    a = P.sample(f'{__base_var_name__}|model_sub@0', PD.Normal(_a_params[0],
        TNF.softplus(_a_params[1])))
    if n == 0:
        return 0.0
    else:
        _yn_params = _nn_model_sub_yn(_hidden)
        yn = P.sample(f'{__base_var_name__}|model_sub@1@0', PD.Normal(
            _yn_params[0], TNF.softplus(_yn_params[1])))
        _site0 = model_sub(_nn__site0(T.cat([_hidden, T.tensor([yn])])), n -
            1, __base_var_name__=f'{__base_var_name__}|model_sub@1@1')
        return _site0 + yn


def model(obs=0.0, __base_var_name__=''):
    P.module('_nn_model_sub_a', _nn_model_sub_a)
    P.module('_nn_model_sub_yn', _nn_model_sub_yn)
    P.module('_nn__site0', _nn__site0)
    P.module('_nn_model_n', _nn_model_n)
    P.module('_nn_z', _nn_z)
    _n_params = _nn_model_n(T.tensor([obs]))
    n = P.sample(f'{__base_var_name__}|model@0', PD.Categorical(T.sigmoid(T
        .reshape(_n_params, (5,)))))
    z = model_sub(_nn_z(T.tensor([obs])), n, __base_var_name__=
        f'{__base_var_name__}|model@1')
    return T.tensor(obs)
