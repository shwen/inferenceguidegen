import pyro as P
import pyro.distributions as PD
import torch as T
import igg


def model_sub(n=0, __base_var_name__=''):
    a = P.sample(f'{__base_var_name__}|model_sub@0', PD.Normal(1.1, 0.1))
    if n == 0:
        return 0.0
    else:
        yn = P.sample(f'{__base_var_name__}|model_sub@1@0', PD.Normal(1.0, 0.1)
            )
        return model_sub(n - 1, __base_var_name__=
            f'{__base_var_name__}|model_sub@1@1') + yn


def model(obs=0.0, __base_var_name__=''):
    n = P.sample(f'{__base_var_name__}|model@0', PD.Categorical(T.tensor([
        0.5, 0.5, 0.5, 0.5, 0.5])))
    z = model_sub(n, __base_var_name__=f'{__base_var_name__}|model@1')
    return P.sample(f'{__base_var_name__}|model@2', PD.Normal(z, 0.1), obs=
        T.tensor(obs))
