import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model_sub(n = 0):
    a = P.sample(PD.Normal(1.1,0.1))
    if n == 0:
        P.sample(PD.Normal(4.0,0.1)) + P.sample(PD.Normal(5.0,0.1))
        return 0.0
    elif n == 1:
        if n > 1:
            P.sample(PD.Normal(10.0,0.1))
        return  P.sample(PD.Normal(2.0,0.1))
    else:
        yn = P.sample(PD.Normal(1.0,0.1))
        return model_sub(n-1) + yn

def model(obs = 0.0):
    n = P.sample(PD.Categorical(T.tensor([0.5, 0.5, 0.5, 0.5, 0.5])))
    z = model_sub(n)
    return P.sample(PD.Normal(z, 0.1), obs=T.tensor(obs))