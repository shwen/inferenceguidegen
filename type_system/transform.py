from pathlib import Path
import ast, astor

class VarNameCreator:

    def __init__(self, tree, output_path):

        self.labels = {}
        self.counter = 0
        self.label_expr_cache = []
        self.user_defined_funcs = []
        self.user_defined_funcs_names = []
        self.tree = tree
        self.output_path = output_path
        for expr in tree.body:
            if isinstance(expr, ast.FunctionDef):
                self.user_defined_funcs.append(expr)
                self.user_defined_funcs_names.append(expr.name)

    # return a label by concatenating suffix to base_name
    def get_label(self, base_name, suffix=None):
        suffix = "" if suffix is None or "" else f"@{suffix}"
        label = base_name + suffix
        return label

    def reset_label_expr_cache(self):
        self.label_expr_cache = []

    # map a expression to a label    
    def assign_label(self, expr, label):
        key = (len(self.labels), expr)
        self.labels[key] = label 

    # store all pyro.sample statements and function calls in the same expression to cache
    def scan_label_statement(self, expr):
        if isinstance(expr, ast.Assign):
            return self.scan_label_statement(expr.value)
        elif isinstance(expr, ast.Call):
            if (isinstance(expr.func, ast.Attribute) and 
               (expr.func.value.id == "P" or expr.func.value.id == "pyro") and 
               (expr.func.attr == "sample")): # is a sample statement
               self.label_expr_cache.append(expr)
            else:
                if isinstance(expr.func, ast.Name) and expr.func.id in self.user_defined_funcs_names:
                    self.label_expr_cache.append(expr) # expr is a possible recursive call
                for arg in expr.args:
                    self.scan_label_statement(arg)
        elif isinstance(expr, ast.BinOp):
            self.scan_label_statement(expr.left)
            self.scan_label_statement(expr.right)
        
        elif isinstance(expr, ast.Return):
            self.scan_label_statement(expr.value)
        
        elif isinstance(expr, ast.Expr):
            self.scan_label_statement(expr.value)

    # create labels for targeted statements (function calls, sample statements)
    def label_creation(self, base_name, exprList, init_counter = 0, else_if = False):
        """
        assumptions:
        1. pyro.sample statements will not be the condition of if statements
        2. all user defined functions are in one python file

        """
        counter = init_counter
        for element in exprList:
            self.reset_label_expr_cache()
            #print(ast.dump(element), type(element))
            if isinstance(element, ast.If):
                
                # recursively create labels in if block
                if else_if:
                    new_base = base_name
                else:
                    new_base = self.get_label(base_name, suffix=counter)
                counter += 1 
                
                self.label_creation(base_name=new_base,
                                    exprList=element.body)

                # check else block
                self.label_creation(base_name=new_base,
                                    exprList=element.orelse,
                                    else_if=(len(element.orelse) > 0 and isinstance(element.orelse[0], ast.If)))                    
                
                continue
            # check the potential target expressions
            self.scan_label_statement(element)

            if self.label_expr_cache: # the expression contain sample statement
                for i, statement in enumerate(self.label_expr_cache):
                    label = self.get_label(base_name, suffix=counter)
                    counter += 1
                    self.assign_label(statement, label)
    
    # create label for all function definitions in the tree
    def generate(self):
        for func in self.user_defined_funcs:
            self.label_creation(f"|{func.name}", func.body)

    def print_labels(self):
        for k, v in self.labels.items():
            print(ast.dump(k[1]), "=>", v)

    # generate strings for random variabels to match pyro requirements
    def create_strings(self):
        for func in self.user_defined_funcs:
            arg_name = '__base_var_name__'
            func.args.args.append(ast.arg(arg=arg_name, annotation=None, type_comment=None))
            func.args.defaults.append(ast.Constant(value='', kind=None))
        for labeled_call, label in self.labels.items():
            if isinstance(labeled_call[1].func, ast.Attribute):
                if ((labeled_call[1].func.value.id == 'P' or labeled_call[1].func.value.id == 'pyro') and
                    (labeled_call[1].func.attr == 'sample')):
                    # is a sample statement
                    var_name = ast.BinOp(left=ast.Name(id=arg_name, ctx=ast.Load()), op=ast.Add(), right=ast.Constant(value=label, kind=None))
                    # use formateted string because igg does not like BinOp Add
                    var_name = ast.JoinedStr(values=[ast.FormattedValue(value=ast.Name(id=arg_name, ctx=ast.Load()), conversion=-1, format_spec=None), ast.Constant(value=label, kind=None)])
                    # add to the first argument
                    labeled_call[1].args.insert(0, var_name)
                else:
                    raise ValueError("unrecognized label call")
            elif isinstance(labeled_call[1].func, ast.Name):
                # calling other functions
                var_name = ast.BinOp(left=ast.Name(id=arg_name, ctx=ast.Load()), op=ast.Add(), right=ast.Constant(value=label, kind=None))
                # use formateted string because igg does not like BinOp Add
                var_name = ast.JoinedStr(values=[ast.FormattedValue(value=ast.Name(id=arg_name, ctx=ast.Load()), conversion=-1, format_spec=None), ast.Constant(value=label, kind=None)])
                # add to the last argument by specifying keyword
                labeled_call[1].keywords.append(ast.keyword(arg=arg_name, value=var_name))

    # output python code from tree
    def generate_source_from_tree(self):
        with open(self.output_path, 'w') as f:
            f.write(astor.to_source(self.tree))

def main(input_path, output_path):
    file_in = Path(input_path)
    with open(file_in, 'rb') as f:
        tree = ast.parse(f.read(), filename=file_in.name)
        varNameCreator = VarNameCreator(tree, output_path)
        varNameCreator.generate()
        varNameCreator.print_labels()
        varNameCreator.create_strings()
        varNameCreator.generate_source_from_tree()

if __name__ == '__main__':
    
    import argparse
    # we may want to change the name
    parser = argparse.ArgumentParser(description='TypeScript Pyro String Name Transformer')
    parser.add_argument("--input", type=str,
                        help="Input file path", default="input.py")
    parser.add_argument("--output", type=str,
                        help="Output file path", default="output.py")
    args = parser.parse_args()

    main(args.input, args.output)

