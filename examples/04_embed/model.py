import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model_sub(n = 0):
    if n == 0:
        return 0.0
    else:
        wn = P.sample(f"y{n}", PD.Normal(1.0,0.1))
        """%
        yn
        EMBED (5,) PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](1, 5),
            PN.PyroModule[TN.Sigmoid](),
            PN.PyroModule[TN.Linear](5, 5));
        %"""
        yn = P.sample(f"y{n}", PD.Normal(wn,0.01))
        "%lhs SHAPE ();%"
        lhs = model_sub(n-1)
        return lhs + yn

def model(obs = 0.0):
    n = P.sample("n", PD.Categorical(T.tensor([0.5, 0.5, 0.5, 0.5, 0.5])))
    z = model_sub(n)
    return P.sample("x", PD.Normal(z, 0.1), obs=T.tensor(obs))
