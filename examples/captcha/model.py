import pyro as P
import pyro.distributions as PD
import torch as T
import string
from utils import render_image
from hyperparam import prior_num_p, prior_noise_mean, prior_noise_sig, prior_num_c_i, \
                       MAX_N, MIN_N, MAX_NOISE, MIN_NOISE, POSSIBLE_N, \
                       noise_constraint, char_dict

def _map_to_noise_range(input):
    input = T.distributions.transform_to(noise_constraint)(input)
    return input

def aggregate(char_i, rhs):
    # we may want to import these global hyperparameters but can't do rn
    char_dict = "0123456789"
    return char_dict[char_i] + rhs

def model_char(i, num_char):
    if i - 2 < num_char:
        # we want to use prior_num_c_i to replace [0.1, ...., 0.1]
        char_i = P.sample(f"char_{i}", PD.Categorical(T.tensor([0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]).cuda()))
        rhs = model_char(i + 1, num_char)
        return aggregate(char_i, rhs)
    else:
        return ""

def model(observations):
    
    "%obs SHAPE (24, 60);%"
    obs = observations['obs']
    #print("model", obs.shape)
    # sample the number of characters
    num_char = P.sample("num_char", PD.Categorical(T.tensor([0.333, 0.333, 0.333]).cuda()))
    #N_index = num_char + 2 # MIN_N

    # sample the noise
    noise = P.sample("noise", PD.Normal(T.tensor(0.5).cuda(), T.tensor(0.5).cuda()))
    noise_trimmed = _map_to_noise_range(noise)
            
    text = model_char(i=0, num_char=num_char)
    gen_image = render_image(text, noise_trimmed)
    return P.sample("obs", PD.Normal(gen_image, 0.00001), obs=obs)
