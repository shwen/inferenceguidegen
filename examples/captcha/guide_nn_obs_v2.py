import torch as T
import torch.distributions as TD
import torch.distributions.constraints as TDC
import torch.nn as TN
import torch.nn.functional as TNF
import pyro as P
import pyro.infer as PI
import pyro.optim as PO
import pyro.distributions as PD
import pyro.nn as PN
import pyro as P
import pyro.distributions as PD
import torch as T
import string
from utils import render_image
from hyperparam import prior_num_p, prior_noise_mean, prior_noise_sig, prior_num_c_i, \
                       MAX_N, MIN_N, MAX_NOISE, MIN_NOISE, POSSIBLE_N, \
                       noise_constraint, char_dict

# manual changes:
# add cuda
# change neural network capacity
# change activation to ReLU
# change output activation for categorical from Sigmoid to LogSoftmax  + 1e-8 to avoid 0 logits
cuda = T.device('cuda')

_nn_model_char_char_i = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.
    Linear](512 + 640, 1600), PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](
    1600, 1024), PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](1024, 10)).cuda()
_nn_rhs = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](513, 512), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](512, 512)).cuda()
_nn_model_noise = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](
    640, 1024), PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](1024,
    1024), PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](1024, 2)).cuda()
_nn_model_num_char = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](
    640, 1024), PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](1024,
    512), PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](512, 3)).cuda()
_nn_text = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](641, 512),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](512, 512)).cuda()

def model_char(obs, _hidden, i, num_char):
    P.module('_nn_model_char_char_i', _nn_model_char_char_i)
    P.module('_nn_rhs', _nn_rhs)
    if i - 2 < num_char:
        _char_i_params = _nn_model_char_char_i(T.cat([T.flatten(obs), _hidden]))
        char_i = P.sample(f'char_{i}', PD.Categorical(TN.LogSoftmax()(T.reshape(
            _char_i_params, (10,))  + 1e-8)))
        rhs = model_char(obs, _nn_rhs(T.cat([_hidden, T.tensor([char_i]).cuda()])), i +
            1, num_char)
        return aggregate(char_i, rhs)
    else:
        return ''


def model(observations):
    P.module('_nn_model_noise', _nn_model_noise)
    P.module('_nn_model_num_char', _nn_model_num_char)
    P.module('_nn_text', _nn_text)
    obs = observations["obs"]
    _noise_params = _nn_model_noise(T.flatten(obs))
    noise = P.sample('noise', PD.Normal(_noise_params[0], TNF.softplus(
        _noise_params[1])))
    noise_trimmed = _map_to_noise_range(noise)
    _num_char_params = _nn_model_num_char(T.flatten(obs))
    num_char = P.sample('num_char', PD.Categorical(TN.LogSoftmax()(T.reshape(
        _num_char_params, (3,)) + 1e-8)))
    text = model_char(obs, _nn_text(T.cat([T.flatten(obs), T.tensor([num_char]).cuda()] # try trainable param init for hidden state
        )), 0, num_char)
    #gen_image = render_image(text, noise_trimmed)
    return obs


def _map_to_noise_range(input):
    input = T.distributions.transform_to(noise_constraint)(input)
    return input


def aggregate(char_i, rhs):
    char_dict = '0123456789'
    return char_dict[char_i] + rhs
