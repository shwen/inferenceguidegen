import torch as T
import torch.distributions as TD
import torch.distributions.constraints as TDC
import torch.nn as TN
import torch.nn.functional as TNF
import pyro as P
import pyro.infer as PI
import pyro.optim as PO
import pyro.distributions as PD
import pyro.nn as PN
import pyro as P
import pyro.distributions as PD
import torch as T
import string
from utils import render_image
from hyperparam import prior_num_p, prior_noise_mean, prior_noise_sig, prior_num_c_i, \
                       MAX_N, MIN_N, MAX_NOISE, MIN_NOISE, POSSIBLE_N, \
                       noise_constraint, char_dict

# manual changes:
# add cuda
# change neural network capacity
# change activation to ReLU
# change output activation for categorical from Sigmoid to LogSoftmax  + 1e-8 to avoid 0 logits
cuda = T.device('cuda')

hidden_size = 719

class LSTM_WRAP(TN.Module):
    def __init__(self):
        super().__init__()
        self.lstm = TN.LSTMCell(643, hidden_size) # input: flatten observation, dist_type, last sampling value
        self.h_0 = TN.Parameter(T.zeros(1, hidden_size))
        self.c_0 = TN.Parameter(T.zeros(1, hidden_size))

    def forward(self, x):
        return None

lstm_wrap = LSTM_WRAP().cuda()
lstm = lstm_wrap.lstm
h_0 = lstm_wrap.h_0
c_0 = lstm_wrap.c_0

start_sampling_value = TN.Parameter(T.zeros(1)).cuda()

_nn_model_char_char_i = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.
    Linear](hidden_size, 1197), PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](1197, 1024),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](1024, 10)).cuda()

_nn_model_noise = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.
    Linear](hidden_size, 1024), PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](1024, 2)).cuda()

_nn_model_num_char = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.
    Linear](hidden_size, 1024), PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](1024, 3)).cuda()

def construct_input(obs, dist_type, last_sampling_value):
    dist_type = TNF.one_hot(T.tensor(dist_type).cuda(), 2).unsqueeze(0)
    return T.cat([T.flatten(obs).unsqueeze(0), dist_type, last_sampling_value.unsqueeze(0)], -1)

def model_char(obs, i, num_char, h_x, c_x):
    P.module('_nn_model_char_char_i', _nn_model_char_char_i)
    if i - 2 < num_char:
        input_x = construct_input(obs, 1, T.tensor([num_char]).cuda())
        h_x,c_x = lstm(input_x, (h_x, c_x))
        _char_i_params = _nn_model_char_char_i(h_x[0])
        char_i = P.sample(f'char_{i}', PD.Categorical(TN.LogSoftmax()(T.reshape(
            _char_i_params, (10,))  + 1e-8)))
        rhs = model_char(obs, i + 1, num_char, h_x, c_x)
        return aggregate(char_i, rhs)
    else:
        return ''

def model(observations):
    P.module('lstm', lstm_wrap)
    P.module('_nn_model_noise', _nn_model_noise)
    P.module('_nn_model_num_char', _nn_model_num_char)

    obs = observations["obs"]

    input_x = construct_input(obs, 0, start_sampling_value)

    h_x,c_x = lstm(input_x, (h_0, c_0))

    _noise_params = _nn_model_noise(h_x[0])
    noise = P.sample('noise', PD.Normal(_noise_params[0], TNF.softplus(_noise_params[1])))

    input_x = construct_input(obs, 1, T.tensor([noise]).cuda())

    h_x,c_x = lstm(input_x, (h_x, c_x))

    _num_char_params = _nn_model_num_char(h_x[0])
    num_char = P.sample('num_char', PD.Categorical(TN.LogSoftmax()(T.reshape(
        _num_char_params, (3,)) + 1e-8)))
    
    text = model_char(obs, 0, num_char, h_x, c_x)
    return obs

def _map_to_noise_range(input):
    input = T.distributions.transform_to(noise_constraint)(input)
    return input


def aggregate(char_i, rhs):
    char_dict = '0123456789'
    return char_dict[char_i] + rhs
