import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model(obs = 0.0):
    q = P.sample("q", PD.Bernoulli(0.5))
    if q > 0:
        y = P.sample("y", PD.Normal(5.0,1.0))
        return P.sample("obs", PD.Normal(y, 0.1), obs=T.tensor(obs))
    else:
        return P.sample("obs", PD.Normal(-5.0, 0.1), obs=T.tensor(obs))

