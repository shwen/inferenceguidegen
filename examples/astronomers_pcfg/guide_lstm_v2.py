import torch as T
import torch.distributions as TD
import torch.distributions.constraints as TDC
import torch.nn as TN
import torch.nn.functional as TNF
import pyro as P
import pyro.infer as PI
import pyro.optim as PO
import pyro.distributions as PD
import pyro.nn as PN
from utils import *

sample_embedding_dim = 6

_nn_model_sample_parse_tree_production_index_lstm = TN.LSTMCell(100 + sample_embedding_dim + 6, 100)

_nn_model_sample_parse_tree_production_index_VP = PN.PyroModule[TN.Sequential](
    PN.PyroModule[TN.Linear](100, 124), PN.PyroModule[TN.ReLU](), PN.
    PyroModule[TN.Linear](124, 124), PN.PyroModule[TN.ReLU](), PN.
    PyroModule[TN.Linear](124, 2))
_nn_model_sample_parse_tree_production_index_NP = PN.PyroModule[TN.Sequential](
    PN.PyroModule[TN.Linear](100, 124), PN.PyroModule[TN.ReLU](), PN.
    PyroModule[TN.Linear](124, 124), PN.PyroModule[TN.ReLU](), PN.
    PyroModule[TN.Linear](124, 6))

def model_S(_obs, _hidden, last_sample_emb, max_depth_parse_tree=20, terminate_symbol='__',
    terminals='', depth=0, suffix='root', production_index=0, hx=None):
    ind = int(production_index)
    if ind == 0:
        l2_S = model_sample_parse_tree(_obs, _hidden, last_sample_emb,
            max_depth_parse_tree, terminate_symbol, terminals, 'VP', depth +
            1, new_suffix(suffix, depth, 1), hx)
        l1_S = model_sample_parse_tree(_obs, _hidden, last_sample_emb, max_depth_parse_tree, terminate_symbol,
            terminals, 'NP', depth + 1, new_suffix(suffix, depth, 0), hx)
        return to_list('S') + to_list(l1_S) + to_list(l2_S)


def model_NP(_obs, _hidden, last_sample_emb, max_depth_parse_tree=20, terminate_symbol='__',
    terminals='', depth=0, suffix='root', production_index=0, hx=None):
    ind = int(production_index)
    if ind == 0:
        l1_NP = model_sample_parse_tree(_obs, _hidden, last_sample_emb,
            max_depth_parse_tree, terminate_symbol, terminals, 'NP', depth +
            1, new_suffix(suffix, depth, 0), hx)
        l2_NP = model_sample_parse_tree(_obs, _hidden, last_sample_emb, max_depth_parse_tree,
            terminate_symbol, terminals, 'PP', depth + 1, new_suffix(suffix,
            depth, 1), hx)
        return to_list('NP') + to_list(l1_NP) + to_list(l2_NP)
    elif ind == 1:
        _site0 = model_sample_parse_tree(_obs, _hidden, last_sample_emb,
            max_depth_parse_tree, terminate_symbol, terminals,
            'astronomers', depth + 1, new_suffix(suffix, depth, 0), hx)
        return to_list('NP') + to_list(_site0)
    elif ind == 2:
        _site1 = model_sample_parse_tree(_obs, _hidden, last_sample_emb,
            max_depth_parse_tree, terminate_symbol, terminals, 'ears', 
            depth + 1, new_suffix(suffix, depth, 0), hx)
        return to_list('NP') + to_list(_site1)
    elif ind == 3:
        _site2 = model_sample_parse_tree(_obs, _hidden, last_sample_emb,
            max_depth_parse_tree, terminate_symbol, terminals, 'saw', depth +
            1, new_suffix(suffix, depth, 0), hx)
        return to_list('NP') + to_list(_site2)
    elif ind == 4:
        _site3 = model_sample_parse_tree(_obs, _hidden, last_sample_emb,
            max_depth_parse_tree, terminate_symbol, terminals, 'stars', 
            depth + 1, new_suffix(suffix, depth, 0), hx)
        return to_list('NP') + to_list(_site3)
    elif ind == 5:
        _site4 = model_sample_parse_tree(_obs, _hidden, last_sample_emb,
            max_depth_parse_tree, terminate_symbol, terminals, 'telescopes',
            depth + 1, new_suffix(suffix, depth, 0), hx)
        return to_list('NP') + to_list(_site4)


def model_VP(_obs, _hidden, last_sample_emb, max_depth_parse_tree=20, terminate_symbol='__',
    terminals='', depth=0, suffix='root', production_index=0, hx=None):
    ind = int(production_index)
    if ind == 0:
        l1_VP_0 = model_sample_parse_tree(_obs, _hidden, last_sample_emb,
            max_depth_parse_tree, terminate_symbol, terminals, 'V', depth +
            1, new_suffix(suffix, depth, 0), hx)
        l2_VP_0 = model_sample_parse_tree(_obs, _hidden, last_sample_emb, max_depth_parse_tree,
            terminate_symbol, terminals, 'NP', depth + 1, new_suffix(suffix,
            depth, 1), hx)
        return to_list('VP') + to_list(l1_VP_0) + to_list(l2_VP_0)
    elif ind == 1:
        l1_VP_1 = model_sample_parse_tree(_obs, _hidden, last_sample_emb,
            max_depth_parse_tree, terminate_symbol, terminals, 'VP', depth +
            1, new_suffix(suffix, depth, 0), hx)
        l2_VP_1 = model_sample_parse_tree(_obs, _hidden, last_sample_emb, max_depth_parse_tree,
            terminate_symbol, terminals, 'PP', depth + 1, new_suffix(suffix,
            depth, 1), hx)
        return to_list('VP') + to_list(l1_VP_1) + to_list(l2_VP_1)


def model_PP(_obs, _hidden, last_sample_emb, max_depth_parse_tree=20, terminate_symbol='__',
    terminals='', depth=0, suffix='root', production_index=0, hx=None):
    ind = int(production_index)
    if ind == 0:
        l2_PP = model_sample_parse_tree(_obs, _hidden, last_sample_emb,
            max_depth_parse_tree, terminate_symbol, terminals, 'NP', depth +
            1, new_suffix(suffix, depth, 1), hx)
        l1_PP = model_sample_parse_tree(_obs, _hidden, last_sample_emb, max_depth_parse_tree,
            terminate_symbol, terminals, 'P', depth + 1, new_suffix(suffix,
            depth, 0), hx)
        return to_list('PP') + to_list(l1_PP) + to_list(l2_PP)


def model_P(_obs, _hidden, last_sample_emb, max_depth_parse_tree=20, terminate_symbol='__',
    terminals='', depth=0, suffix='root', production_index=0, hx=None):
    ind = int(production_index)
    if ind == 0:
        _site0 = model_sample_parse_tree(_obs, _hidden, last_sample_emb,
            max_depth_parse_tree, terminate_symbol, terminals, 'with', 
            depth + 1, new_suffix(suffix, depth, 0), hx)
        return to_list('P') + to_list(_site0)


def model_V(_obs, _hidden, last_sample_emb, max_depth_parse_tree=20, terminate_symbol='__',
    terminals='', depth=0, suffix='root', production_index=0, hx=None):
    ind = int(production_index)
    if ind == 0:
        _site0 = model_sample_parse_tree(_obs, _hidden, last_sample_emb,
            max_depth_parse_tree, terminate_symbol, terminals, 'saw', depth +
            1, new_suffix(suffix, depth, 0), hx)
        return to_list('V') + to_list(_site0)


def model_sample_parse_tree(_obs, _hidden, last_sample_emb, max_depth_parse_tree=20,
    terminate_symbol='__', terminals='', symbol='S', depth=0, suffix='root', hx=None):
    P.module('_nn_model_sample_parse_tree_production_index_VP',
        _nn_model_sample_parse_tree_production_index_VP)
    P.module('_nn_model_sample_parse_tree_production_index_NP',
        _nn_model_sample_parse_tree_production_index_NP)
    P.module('_nn_model_sample_parse_tree_production_index_lstm',
        _nn_model_sample_parse_tree_production_index_lstm)

    if terminated(symbol, terminals):
        return symbol
    elif depth > max_depth_parse_tree:
        return terminate_symbol
    elif symbol == 'S':
        sample_address_embedding = get_sample_address_embedding(
                symbol)
        input = T.cat([_obs, last_sample_emb, sample_address_embedding], 0).unsqueeze(0)
        hn, cn = _nn_model_sample_parse_tree_production_index_lstm(input, hx)
        
        production_index_S = P.sample(f'production_index_S_{depth}_{suffix}',
            PD.Categorical(logits=T.tensor([1.])))

        sample_emb = one_hot(T.tensor([production_index_S]), sample_embedding_dim)[0]
        return model_S(_obs, _hidden, sample_emb, max_depth_parse_tree, terminate_symbol, terminals,
            depth, suffix, production_index_S, (hn, cn))
    elif symbol == 'NP':
        sample_address_embedding = get_sample_address_embedding(
                symbol)
        input = T.cat([_obs, last_sample_emb, sample_address_embedding], 0).unsqueeze(0)
        hn, cn = _nn_model_sample_parse_tree_production_index_lstm(input, hx)
        _production_index_NP_params = (
            _nn_model_sample_parse_tree_production_index_NP(hn[0]))
        production_index_NP = P.sample(f'production_index_NP_{depth}_{suffix}',
            PD.Categorical(logits=TN.LogSoftmax()(T.reshape(_production_index_NP_params,
            (6,)))+ 1e-8))
        sample_emb = one_hot(T.tensor([production_index_NP]), sample_embedding_dim)[0]
        return model_NP(_obs, _hidden, sample_emb, max_depth_parse_tree, terminate_symbol, terminals,
            depth, suffix, production_index_NP, (hn, cn))
    elif symbol == 'VP':
        sample_address_embedding = get_sample_address_embedding(
                symbol)
        input = T.cat([_obs, last_sample_emb, sample_address_embedding], 0).unsqueeze(0)
        hn, cn = _nn_model_sample_parse_tree_production_index_lstm(input, hx)
        _production_index_VP_params = (
            _nn_model_sample_parse_tree_production_index_VP(hn[0]))
        production_index_VP = P.sample(f'production_index_VP_{depth}_{suffix}',
            PD.Categorical(logits=TN.LogSoftmax()(T.reshape(_production_index_VP_params,
            (2,)))+ 1e-8))
        sample_emb = one_hot(T.tensor([production_index_VP]), sample_embedding_dim)[0]
        return model_VP(_obs, _hidden, sample_emb, max_depth_parse_tree, terminate_symbol, terminals,
            depth, suffix, production_index_VP, (hn, cn))
    elif symbol == 'PP':
        sample_address_embedding = get_sample_address_embedding(
                symbol)
        input = T.cat([_obs, last_sample_emb, sample_address_embedding], 0).unsqueeze(0)
        hn, cn = _nn_model_sample_parse_tree_production_index_lstm(input, hx)
        production_index_PP = P.sample(f'production_index_PP_{depth}_{suffix}',
            PD.Categorical(logits=T.tensor([1.])))
        sample_emb = one_hot(T.tensor([production_index_PP]), sample_embedding_dim)[0]
        return model_PP(_obs, _hidden, sample_emb, max_depth_parse_tree, terminate_symbol, terminals,
            depth, suffix, production_index_PP, (hn, cn))
    elif symbol == 'P':
        sample_address_embedding = get_sample_address_embedding(
                symbol)
        input = T.cat([_obs, last_sample_emb, sample_address_embedding], 0).unsqueeze(0)
        hn, cn = _nn_model_sample_parse_tree_production_index_lstm(input, hx)
        production_index_P = P.sample(f'production_index_P_{depth}_{suffix}',
            PD.Categorical(logits=T.tensor([1.])))
        sample_emb = one_hot(T.tensor([production_index_P]), sample_embedding_dim)[0]
        return model_P(_obs, _hidden, sample_emb, max_depth_parse_tree, terminate_symbol, terminals,
            depth, suffix, production_index_P, (hn, cn))
    elif symbol == 'V':
        sample_address_embedding = get_sample_address_embedding(
                symbol)
        input = T.cat([_obs, last_sample_emb, sample_address_embedding], 0).unsqueeze(0)
        hn, cn = _nn_model_sample_parse_tree_production_index_lstm(input, hx)
        production_index_V = P.sample(f'production_index_V_{depth}_{suffix}',
            PD.Categorical(logits=T.tensor([1.])))
        sample_emb = one_hot(T.tensor([production_index_V]), sample_embedding_dim)[0]
        return model_V(_obs, _hidden, sample_emb, max_depth_parse_tree, terminate_symbol, terminals,
            depth, suffix, production_index_V, (hn, cn))


def model(start_symbol='S', terminals=["astronomers", "ears", "saw", "stars", "telescopes", "with"], max_depth_parse_tree=20,
    terminate_symbol='__', observations={'obs': 0}):
    tree = model_sample_parse_tree(observations['obs'], observations['obs']
        , T.zeros(sample_embedding_dim), max_depth_parse_tree, terminate_symbol, terminals, start_symbol,
        0, 'root')
    #sentence = get_leaves(tree, max_depth_parse_tree, terminate_symbol)
    # pad_length = max(len(sentence), len(observations['obs']))
    # obs_sentence = pad_sentence(observations['obs'], pad_length)
    # gen_sentence = pad_sentence(sentence_to_one_hots(sentence, terminals),
    #     pad_length)
    return tree
    #return observations['obs']


def terminated(symbol, terminals):
    return symbol in terminals


def get_production(symbol, production_index, production_rules):
    return production_rules[symbol][production_index]


def to_list(x):
    return [x]


def new_suffix(old_suffix, depth, suffix_ind):
    return f'{old_suffix}_d{depth}/{suffix_ind}'


def get_obs(observations):
    return observations['obs']
"""
manual fix:
    in model: 
    1. obs=obs_sentence
    in guide:
    1. remove tensor before observations['obs']
    2. change nn_tree to input size 100 instead of 1
    3. comment out pad_length -> gen-sentence
    4. add _hidden to the first argument of model_S/model_V/...
    5. _nn_site_0 should have function name as its suffix
"""