
# example:
# tree = ['S', ['NP', ['NP', 'saw'], ['PP', ['P', 'with'], ['NP', ['NP', 'telescopes'], ['PP', ['P', 'with'], ['NP', 'astronomers']]]]], ['VP', ['V', 'saw'], ['NP', 'astronomers']]]
# sentence = ['saw', 'with', 'telescopes', 'with', 'astronomers', 'saw', 'astronomers']

# pcfg = {
#   "name": "astronomers",
#   "terminals": ["astronomers", "ears", "saw", "stars", "telescopes", "with"],
#   "non_terminals": ["S", "NP", "VP", "PP", "P", "V"],
#   "productions": {
#     "S": [["NP", "VP"]],
#     "NP": [["NP", "PP"], ["astronomers"], ["ears"], ["saw"], ["stars"], ["telescopes"]],
#     "VP": [["V", "NP"], ["VP", "PP"]],
#     "PP": [["P", "NP"]],
#     "P": [["with"]],
#     "V": [["saw"]]
#   },
#   "start_symbol": "S",
# }
# true_production_probs = {
#     "S": [1.0],
#     "NP": [0.4, 0.1, 0.18, 0.04, 0.18, 0.1],
#     "VP": [0.7, 0.3],
#     "PP": [1.0],
#     "P": [1.0],
#     "V": [1.0]
# }
# max_depth_parse_tree = 20

# terminate_symbol = "__"

import pyro as P
import pyro.distributions as PD
import torch as T
import numpy as np
from utils import *

def terminated(symbol, terminals):
    return symbol in terminals

def get_production(symbol, production_index, production_rules):
    return production_rules[symbol][production_index]

def to_list(x):
    return [x]

def new_suffix(old_suffix, depth, suffix_ind):
    return f"{old_suffix}_d{depth}/{suffix_ind}"

def get_obs(observations):
    return observations['obs']
def model_S(max_depth_parse_tree=20, terminate_symbol="__", terminals="", depth=0, suffix="root", production_index=0):
    ind = int(production_index)
    if ind == 0:
        """%
        l1_S
        EMBED (100,) convert_parse_tree_to_tensor
        %"""
        l1_S = model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "NP", depth + 1, new_suffix(suffix, depth, 0))
        """%
        l2_S
        EMBED (100,) convert_parse_tree_to_tensor
        %"""
        l2_S = model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "VP", depth + 1, new_suffix(suffix, depth, 1))
        return to_list("S") + to_list(l1_S) + to_list(l2_S)

def model_NP(max_depth_parse_tree=20, terminate_symbol="__", terminals="", depth=0, suffix="root", production_index=0):
    ind = int(production_index)
    if ind == 0:
        """%
        l1_NP
        EMBED (100,) convert_parse_tree_to_tensor
        %"""
        l1_NP = model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "NP", depth + 1, new_suffix(suffix, depth, 0))
        """%
        l2_NP
        EMBED (100,) convert_parse_tree_to_tensor
        %"""
        l2_NP = model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "PP", depth + 1, new_suffix(suffix, depth, 1))
        return to_list("NP") + to_list(l1_NP) + to_list(l2_NP)

    elif ind == 1:
        return to_list("NP") + to_list(model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "astronomers", depth + 1, new_suffix(suffix, depth, 0)))
    
    elif ind == 2:
        return to_list("NP") + to_list(model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "ears", depth + 1, new_suffix(suffix, depth, 0)))
    
    elif ind == 3:
        return to_list("NP") + to_list(model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "saw", depth + 1, new_suffix(suffix, depth, 0)))

    elif ind == 4:
        return to_list("NP") + to_list(model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "stars", depth + 1, new_suffix(suffix, depth, 0)))
    
    elif ind == 5:
        return to_list("NP") + to_list(model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "telescopes", depth + 1, new_suffix(suffix, depth, 0)))
    
def model_VP(max_depth_parse_tree=20, terminate_symbol="__", terminals="", depth=0, suffix="root", production_index=0):
    ind = int(production_index)
    if ind == 0:
        """%
        l1_VP_0
        EMBED (100,) convert_parse_tree_to_tensor
        %"""
        l1_VP_0 = model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "V", depth + 1, new_suffix(suffix, depth, 0))
        """%
        l2_VP_0
        EMBED (100,) convert_parse_tree_to_tensor
        %"""
        l2_VP_0 = model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "NP", depth + 1, new_suffix(suffix, depth, 1))
        return to_list("VP") + to_list(l1_VP_0) + to_list(l2_VP_0)
    elif ind == 1:
        """%
        l1_VP_1
        EMBED (100,) convert_parse_tree_to_tensor
        %"""
        l1_VP_1 = model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "VP", depth + 1, new_suffix(suffix, depth, 0))
        """%
        l2_VP_1
        EMBED (100,) convert_parse_tree_to_tensor
        %"""
        l2_VP_1 = model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "PP", depth + 1, new_suffix(suffix, depth, 1))
        return to_list("VP") + to_list(l1_VP_1) + to_list(l2_VP_1)

def model_PP(max_depth_parse_tree=20, terminate_symbol="__", terminals="", depth=0, suffix="root", production_index=0):
    ind = int(production_index)
    if ind == 0:
        """%
        l1_PP
        EMBED (100,) convert_parse_tree_to_tensor
        %"""
        l1_PP = model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "P", depth + 1, new_suffix(suffix, depth, 0))
        """%
        l2_PP
        EMBED (100,) convert_parse_tree_to_tensor
        %"""
        l2_PP = model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "NP", depth + 1, new_suffix(suffix, depth, 1))
        return to_list("PP") + to_list(l1_PP) + to_list(l2_PP)

def model_P(max_depth_parse_tree=20, terminate_symbol="__", terminals="", depth=0, suffix="root", production_index=0):
    ind = int(production_index)
    if ind == 0:
        return to_list("P") + to_list(model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "with", depth + 1, new_suffix(suffix, depth, 0)))

def model_V(max_depth_parse_tree=20, terminate_symbol="__", terminals="", depth=0, suffix="root", production_index=0):
    ind = int(production_index)
    if ind == 0:
        return to_list("V") + to_list(model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, "saw", depth + 1, new_suffix(suffix, depth, 0)))

def model_sample_parse_tree(max_depth_parse_tree=20, terminate_symbol="__", terminals="", symbol="S", depth=0, suffix="root"):
    if terminated(symbol, terminals):
        return symbol
    elif depth > max_depth_parse_tree:
        return terminate_symbol
    elif symbol == "S":
        production_index_S = P.sample(f"production_index_S_{depth}_{suffix}", PD.Categorical(logits=T.tensor([1.])))
        return model_S(max_depth_parse_tree, terminate_symbol, terminals, depth, suffix, production_index_S)
    elif symbol == "NP":
        production_index_NP = P.sample(f"production_index_NP_{depth}_{suffix}", PD.Categorical(logits=T.tensor([1., 1. ,1. ,1. ,1., 1.])))
        return model_NP(max_depth_parse_tree, terminate_symbol, terminals, depth, suffix, production_index_NP)
    elif symbol == "VP":
        production_index_VP = P.sample(f"production_index_VP_{depth}_{suffix}", PD.Categorical(logits=T.tensor([1., 1.])))
        return model_VP(max_depth_parse_tree, terminate_symbol, terminals, depth, suffix, production_index_VP)
    elif symbol == "PP":
        production_index_PP = P.sample(f"production_index_PP_{depth}_{suffix}", PD.Categorical(logits=T.tensor([1.])))
        return model_PP(max_depth_parse_tree, terminate_symbol, terminals, depth, suffix, production_index_PP)
    elif symbol == "P":
        production_index_P = P.sample(f"production_index_P_{depth}_{suffix}", PD.Categorical(logits=T.tensor([1.])))
        return model_P(max_depth_parse_tree, terminate_symbol, terminals, depth, suffix, production_index_P)
    elif symbol == "V":
        production_index_V = P.sample(f"production_index_V_{depth}_{suffix}", PD.Categorical(logits=T.tensor([1.])))
        return model_V(max_depth_parse_tree, terminate_symbol, terminals, depth, suffix, production_index_V)

def model(start_symbol="S", terminals=["astronomers", "ears", "saw", "stars", "telescopes", "with"], max_depth_parse_tree=20, terminate_symbol="__", observations={"obs":0}):

    # """%
    #     obs
    #     EMBED (100,) TN.GRU(
    #             input_size=6,
    #             hidden_size=100,
    #             num_layers=1)
    #     %"""
    # obs = get_obs(observations)
    tree = model_sample_parse_tree(max_depth_parse_tree, terminate_symbol, terminals, start_symbol, 0, "root")
    sentence = get_leaves(tree, max_depth_parse_tree, terminate_symbol)
    pad_length = max(len(sentence), len(observations["obs"]))
    sentence = sentence_to_one_hots(sentence, terminals)
    gen_sentence = pad_sentence(sentence, pad_length)
    obs_sentence = pad_sentence(observations["obs"], pad_length)
    P.sample("obs", PD.Normal(gen_sentence, 1e-8), obs=obs_sentence)
    return sentence
    