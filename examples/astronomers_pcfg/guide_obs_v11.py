import torch as T
import torch.distributions as TD
import torch.distributions.constraints as TDC
import torch.nn as TN
import torch.nn.functional as TNF
import pyro as P
import pyro.infer as PI
import pyro.optim as PO
import pyro.distributions as PD
import pyro.nn as PN
from utils import *

_nn_merge_lhs = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](210, 128),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](128, 110))

_nn_model_sample_parse_tree_model_symbol = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](122, 128),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](128, 110))

_nn_model_sample_parse_tree_production_index_VP = PN.PyroModule[TN.Sequential](
    PN.PyroModule[TN.Linear](210, 135), PN.PyroModule[TN.ReLU](), PN.
    PyroModule[TN.Linear](135, 133), PN.PyroModule[TN.ReLU](), PN.
    PyroModule[TN.Linear](133, 2))
_nn_model_sample_parse_tree_production_index_NP = PN.PyroModule[TN.Sequential](
    PN.PyroModule[TN.Linear](210, 135), PN.PyroModule[TN.ReLU](), PN.
    PyroModule[TN.Linear](135, 134), PN.PyroModule[TN.ReLU](), PN.
    PyroModule[TN.Linear](134, 6))

_nn_tree = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](100, 128),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](128, 110))

_embed_model_S_l1_S = convert_parse_tree_to_tensor
_embed_model_S_l2_S = convert_parse_tree_to_tensor
_embed_model_NP_l1_NP = convert_parse_tree_to_tensor
_embed_model_NP_l2_NP = convert_parse_tree_to_tensor
_embed_model_VP_l1_VP_0 = convert_parse_tree_to_tensor
_embed_model_VP_l2_VP_0 = convert_parse_tree_to_tensor
_embed_model_VP_l1_VP_1 = convert_parse_tree_to_tensor
_embed_model_VP_l2_VP_1 = convert_parse_tree_to_tensor
_embed_model_PP_l1_PP = convert_parse_tree_to_tensor
_embed_model_PP_l2_PP = convert_parse_tree_to_tensor


def model_S(_obs, _hidden, max_depth_parse_tree=20, terminate_symbol='__',
    terminals='', depth=0, suffix='root', production_index=0):
    ind = int(production_index)
    if ind == 0:
        l2_S = model_sample_parse_tree(_obs, _hidden,
            max_depth_parse_tree, terminate_symbol, terminals, 'VP', depth +
            1, new_suffix(suffix, depth, 1))
        l1_S = model_sample_parse_tree(_obs, _nn_merge_lhs(T.cat([_embed_model_S_l2_S(
            l2_S), _hidden])), max_depth_parse_tree, terminate_symbol,
            terminals, 'NP', depth + 1, new_suffix(suffix, depth, 0))
        return to_list('S') + to_list(l1_S) + to_list(l2_S)


def model_NP(_obs, _hidden, max_depth_parse_tree=20, terminate_symbol='__',
    terminals='', depth=0, suffix='root', production_index=0):
    ind = int(production_index)
    if ind == 0:
        l1_NP = model_sample_parse_tree(_obs, _hidden,
            max_depth_parse_tree, terminate_symbol, terminals, 'NP', depth +
            1, new_suffix(suffix, depth, 0))
        l2_NP = model_sample_parse_tree(_obs, _nn_merge_lhs(T.cat([_hidden,
            _embed_model_NP_l1_NP(l1_NP)])), max_depth_parse_tree,
            terminate_symbol, terminals, 'PP', depth + 1, new_suffix(suffix,
            depth, 1))
        return to_list('NP') + to_list(l1_NP) + to_list(l2_NP)
    elif ind == 1:
        _site0 = model_sample_parse_tree(_obs, _hidden,
            max_depth_parse_tree, terminate_symbol, terminals,
            'astronomers', depth + 1, new_suffix(suffix, depth, 0))
        return to_list('NP') + to_list(_site0)
    elif ind == 2:
        _site1 = model_sample_parse_tree(_obs, _hidden,
            max_depth_parse_tree, terminate_symbol, terminals, 'ears', 
            depth + 1, new_suffix(suffix, depth, 0))
        return to_list('NP') + to_list(_site1)
    elif ind == 3:
        _site2 = model_sample_parse_tree(_obs, _hidden,
            max_depth_parse_tree, terminate_symbol, terminals, 'saw', depth +
            1, new_suffix(suffix, depth, 0))
        return to_list('NP') + to_list(_site2)
    elif ind == 4:
        _site3 = model_sample_parse_tree(_obs,_hidden,
            max_depth_parse_tree, terminate_symbol, terminals, 'stars', 
            depth + 1, new_suffix(suffix, depth, 0))
        return to_list('NP') + to_list(_site3)
    elif ind == 5:
        _site4 = model_sample_parse_tree(_obs, _hidden,
            max_depth_parse_tree, terminate_symbol, terminals, 'telescopes',
            depth + 1, new_suffix(suffix, depth, 0))
        return to_list('NP') + to_list(_site4)


def model_VP(_obs, _hidden, max_depth_parse_tree=20, terminate_symbol='__',
    terminals='', depth=0, suffix='root', production_index=0):
    
    ind = int(production_index)
    if ind == 0:
        l1_VP_0 = model_sample_parse_tree(_obs, _hidden,
            max_depth_parse_tree, terminate_symbol, terminals, 'V', depth +
            1, new_suffix(suffix, depth, 0))
        l2_VP_0 = model_sample_parse_tree(_obs, _nn_merge_lhs(T.cat([_hidden,
            _embed_model_VP_l1_VP_0(l1_VP_0)])), max_depth_parse_tree,
            terminate_symbol, terminals, 'NP', depth + 1, new_suffix(suffix,
            depth, 1))
        return to_list('VP') + to_list(l1_VP_0) + to_list(l2_VP_0)
    elif ind == 1:
        l1_VP_1 = model_sample_parse_tree(_obs, _hidden,
            max_depth_parse_tree, terminate_symbol, terminals, 'VP', depth +
            1, new_suffix(suffix, depth, 0))
        l2_VP_1 = model_sample_parse_tree(_obs, _nn_merge_lhs(T.cat([_hidden,
            _embed_model_VP_l1_VP_1(l1_VP_1)])), max_depth_parse_tree,
            terminate_symbol, terminals, 'PP', depth + 1, new_suffix(suffix,
            depth, 1))
        return to_list('VP') + to_list(l1_VP_1) + to_list(l2_VP_1)


def model_PP(_obs, _hidden, max_depth_parse_tree=20, terminate_symbol='__',
    terminals='', depth=0, suffix='root', production_index=0):
    ind = int(production_index)
    if ind == 0:
        l2_PP = model_sample_parse_tree(_obs, _hidden,
            max_depth_parse_tree, terminate_symbol, terminals, 'NP', depth +
            1, new_suffix(suffix, depth, 1))
        l1_PP = model_sample_parse_tree(_obs, _nn_merge_lhs(T.cat([_hidden,
            _embed_model_PP_l2_PP(l2_PP)])), max_depth_parse_tree,
            terminate_symbol, terminals, 'P', depth + 1, new_suffix(suffix,
            depth, 0))
        return to_list('PP') + to_list(l1_PP) + to_list(l2_PP)


def model_P(_obs, _hidden, max_depth_parse_tree=20, terminate_symbol='__',
    terminals='', depth=0, suffix='root', production_index=0):
    ind = int(production_index)
    if ind == 0:
        _site0 = model_sample_parse_tree(_obs, _hidden,
            max_depth_parse_tree, terminate_symbol, terminals, 'with', 
            depth + 1, new_suffix(suffix, depth, 0))
        return to_list('P') + to_list(_site0)


def model_V(_obs, _hidden, max_depth_parse_tree=20, terminate_symbol='__',
    terminals='', depth=0, suffix='root', production_index=0):
    ind = int(production_index)
    if ind == 0:
        _site0 = model_sample_parse_tree(_obs, _hidden,
            max_depth_parse_tree, terminate_symbol, terminals, 'saw', depth +
            1, new_suffix(suffix, depth, 0))
        return to_list('V') + to_list(_site0)


def model_sample_parse_tree(_obs, _hidden, max_depth_parse_tree=20,
    terminate_symbol='__', terminals='', symbol='S', depth=0, suffix='root'):
    P.module('_nn_merge_lhs', _nn_merge_lhs)
    P.module('_nn_model_sample_parse_tree_production_index_VP',
        _nn_model_sample_parse_tree_production_index_VP)
    P.module('_nn_model_sample_parse_tree_production_index_NP',
        _nn_model_sample_parse_tree_production_index_NP)
    P.module('_nn_model_sample_parse_tree_model_symbol',
        _nn_model_sample_parse_tree_model_symbol)

    if terminated(symbol, terminals):
        return symbol
    elif depth > max_depth_parse_tree:
        return terminate_symbol
    elif symbol == 'S':
        sample_address_embedding = get_sample_address_embedding(
                symbol)
        production_index_S = P.sample(f'production_index_S_{depth}_{suffix}',
            PD.Categorical(logits=T.tensor([1.])))
        return model_S(_obs, _nn_model_sample_parse_tree_model_symbol(T.cat([_hidden, sample_address_embedding, one_hot(T.tensor([production_index_S]), 6)[0]], -1)), max_depth_parse_tree, terminate_symbol, terminals,
            depth, suffix, production_index_S)
    elif symbol == 'NP':
        sample_address_embedding = get_sample_address_embedding(
                symbol)
        _production_index_NP_params = (
            _nn_model_sample_parse_tree_production_index_NP(T.cat([_obs, _hidden], -1)))
        production_index_NP = P.sample(f'production_index_NP_{depth}_{suffix}',
            PD.Categorical(logits=TN.LogSoftmax()(T.reshape(_production_index_NP_params,
            (6,)))+ 1e-8))
        return model_NP(_obs, _nn_model_sample_parse_tree_model_symbol(T.cat([_hidden, sample_address_embedding, one_hot(T.tensor([production_index_NP]), 6)[0]], -1)), max_depth_parse_tree, terminate_symbol, terminals,
            depth, suffix, production_index_NP)
    elif symbol == 'VP':
        sample_address_embedding = get_sample_address_embedding(
                symbol)
        _production_index_VP_params = (
            _nn_model_sample_parse_tree_production_index_VP(T.cat([_obs, _hidden], -1)))
        production_index_VP = P.sample(f'production_index_VP_{depth}_{suffix}',
            PD.Categorical(logits=TN.LogSoftmax()(T.reshape(_production_index_VP_params,
            (2,)))+ 1e-8))
        return model_VP(_obs, _nn_model_sample_parse_tree_model_symbol(T.cat([_hidden, sample_address_embedding, one_hot(T.tensor([production_index_VP]), 6)[0]], -1)), max_depth_parse_tree, terminate_symbol, terminals,
            depth, suffix, production_index_VP)
    elif symbol == 'PP':
        sample_address_embedding = get_sample_address_embedding(
                symbol)
        production_index_PP = P.sample(f'production_index_PP_{depth}_{suffix}',
            PD.Categorical(logits=T.tensor([1.])))
        return model_PP(_obs, _nn_model_sample_parse_tree_model_symbol(T.cat([_hidden, sample_address_embedding, one_hot(T.tensor([production_index_PP]), 6)[0]], -1)), max_depth_parse_tree, terminate_symbol, terminals,
            depth, suffix, production_index_PP)
    elif symbol == 'P':
        sample_address_embedding = get_sample_address_embedding(
                symbol)
        production_index_P = P.sample(f'production_index_P_{depth}_{suffix}',
            PD.Categorical(logits=T.tensor([1.])))
        return model_P(_obs, _nn_model_sample_parse_tree_model_symbol(T.cat([_hidden, sample_address_embedding, one_hot(T.tensor([production_index_P]), 6)[0]], -1)), max_depth_parse_tree, terminate_symbol, terminals,
            depth, suffix, production_index_P)
    elif symbol == 'V':
        sample_address_embedding = get_sample_address_embedding(
                symbol)
        production_index_V = P.sample(f'production_index_V_{depth}_{suffix}',
            PD.Categorical(logits=T.tensor([1.])))
        return model_V(_obs, _nn_model_sample_parse_tree_model_symbol(T.cat([_hidden, sample_address_embedding, one_hot(T.tensor([production_index_V]), 6)[0]], -1)), max_depth_parse_tree, terminate_symbol, terminals,
            depth, suffix, production_index_V)


def model(start_symbol='S', terminals=["astronomers", "ears", "saw", "stars", "telescopes", "with"], max_depth_parse_tree=20,
    terminate_symbol='__', observations={'obs': 0}):
    P.module('_nn_tree', _nn_tree)
    tree = model_sample_parse_tree(observations['obs'], _nn_tree(observations['obs']
        ), max_depth_parse_tree, terminate_symbol, terminals, start_symbol,
        0, 'root')
    #sentence = get_leaves(tree, max_depth_parse_tree, terminate_symbol)
    # pad_length = max(len(sentence), len(observations['obs']))
    # obs_sentence = pad_sentence(observations['obs'], pad_length)
    # gen_sentence = pad_sentence(sentence_to_one_hots(sentence, terminals),
    #     pad_length)
    return tree
    #return observations['obs']


def terminated(symbol, terminals):
    return symbol in terminals


def get_production(symbol, production_index, production_rules):
    return production_rules[symbol][production_index]


def to_list(x):
    return [x]


def new_suffix(old_suffix, depth, suffix_ind):
    return f'{old_suffix}_d{depth}/{suffix_ind}'


def get_obs(observations):
    return observations['obs']
"""
manual fix:
    in model: 
    1. obs=obs_sentence
    in guide:
    1. remove tensor before observations['obs']
    2. change nn_tree to input size 100 instead of 1
    3. comment out pad_length -> gen-sentence
    4. add _hidden to the first argument of model_S/model_V/...
    5. _nn_site_0 should have function name as its suffix
"""