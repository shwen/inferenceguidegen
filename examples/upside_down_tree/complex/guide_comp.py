import torch as T
import torch.distributions as TD
import torch.distributions.constraints as TDC
import torch.nn as TN
import torch.nn.functional as TNF
import pyro as P
import pyro.infer as PI
import pyro.optim as PO
import pyro.distributions as PD
import pyro.nn as PN
import igg
from gmade import GMADE
_nn_model_k2 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_z4 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_y8 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x16 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x15 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_y7 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x14 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x13 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.
    PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_z3 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_y6 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x12 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x11 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.
    PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_y5 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x10 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x9 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_k1 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_z2 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_y4 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x8 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x7 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.
    PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_y3 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x6 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x5 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_z1 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_y2 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x4 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x3 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_y1 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x2 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x1 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8), PN.PyroModule
    [TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))


def model(observations={'obs': 0}):
    P.module('_nn_model_k2', _nn_model_k2)
    P.module('_nn_model_z4', _nn_model_z4)
    P.module('_nn_model_y8', _nn_model_y8)
    P.module('_nn_model_x16', _nn_model_x16)
    P.module('_nn_model_x15', _nn_model_x15)
    P.module('_nn_model_y7', _nn_model_y7)
    P.module('_nn_model_x14', _nn_model_x14)
    P.module('_nn_model_x13', _nn_model_x13)
    P.module('_nn_model_z3', _nn_model_z3)
    P.module('_nn_model_y6', _nn_model_y6)
    P.module('_nn_model_x12', _nn_model_x12)
    P.module('_nn_model_x11', _nn_model_x11)
    P.module('_nn_model_y5', _nn_model_y5)
    P.module('_nn_model_x10', _nn_model_x10)
    P.module('_nn_model_x9', _nn_model_x9)
    P.module('_nn_model_k1', _nn_model_k1)
    P.module('_nn_model_z2', _nn_model_z2)
    P.module('_nn_model_y4', _nn_model_y4)
    P.module('_nn_model_x8', _nn_model_x8)
    P.module('_nn_model_x7', _nn_model_x7)
    P.module('_nn_model_y3', _nn_model_y3)
    P.module('_nn_model_x6', _nn_model_x6)
    P.module('_nn_model_x5', _nn_model_x5)
    P.module('_nn_model_z1', _nn_model_z1)
    P.module('_nn_model_y2', _nn_model_y2)
    P.module('_nn_model_x4', _nn_model_x4)
    P.module('_nn_model_x3', _nn_model_x3)
    P.module('_nn_model_y1', _nn_model_y1)
    P.module('_nn_model_x2', _nn_model_x2)
    P.module('_nn_model_x1', _nn_model_x1)
    _k2_params = _nn_model_k2(T.tensor([observations['obs']]))
    k2 = P.sample('k2', PD.Normal(_k2_params[0], TNF.softplus(_k2_params[1])))
    _z4_params = _nn_model_z4(T.tensor([k2]))
    z4 = P.sample('z4', PD.Normal(_z4_params[0], TNF.softplus(_z4_params[1])))
    _y8_params = _nn_model_y8(T.tensor([z4]))
    y8 = P.sample('y8', PD.Normal(_y8_params[0], TNF.softplus(_y8_params[1])))
    _x16_params = _nn_model_x16(T.tensor([y8]))
    x16 = P.sample('x16', PD.Normal(_x16_params[0], TNF.softplus(
        _x16_params[1])))
    _x15_params = _nn_model_x15(T.tensor([x16, y8]))
    x15 = P.sample('x15', PD.Normal(_x15_params[0], TNF.softplus(
        _x15_params[1])))
    _y7_params = _nn_model_y7(T.tensor([z4, y8]))
    y7 = P.sample('y7', PD.Normal(_y7_params[0], TNF.softplus(_y7_params[1])))
    _x14_params = _nn_model_x14(T.tensor([y7]))
    x14 = P.sample('x14', PD.Normal(_x14_params[0], TNF.softplus(
        _x14_params[1])))
    _x13_params = _nn_model_x13(T.tensor([y7, x14]))
    x13 = P.sample('x13', PD.Normal(_x13_params[0], TNF.softplus(
        _x13_params[1])))
    _z3_params = _nn_model_z3(T.tensor([k2, z4]))
    z3 = P.sample('z3', PD.Normal(_z3_params[0], TNF.softplus(_z3_params[1])))
    _y6_params = _nn_model_y6(T.tensor([z3]))
    y6 = P.sample('y6', PD.Normal(_y6_params[0], TNF.softplus(_y6_params[1])))
    _x12_params = _nn_model_x12(T.tensor([y6]))
    x12 = P.sample('x12', PD.Normal(_x12_params[0], TNF.softplus(
        _x12_params[1])))
    _x11_params = _nn_model_x11(T.tensor([y6, x12]))
    x11 = P.sample('x11', PD.Normal(_x11_params[0], TNF.softplus(
        _x11_params[1])))
    _y5_params = _nn_model_y5(T.tensor([y6, z3]))
    y5 = P.sample('y5', PD.Normal(_y5_params[0], TNF.softplus(_y5_params[1])))
    _x10_params = _nn_model_x10(T.tensor([y5]))
    x10 = P.sample('x10', PD.Normal(_x10_params[0], TNF.softplus(
        _x10_params[1])))
    _x9_params = _nn_model_x9(T.tensor([y5, x10]))
    x9 = P.sample('x9', PD.Normal(_x9_params[0], TNF.softplus(_x9_params[1])))
    _k1_params = _nn_model_k1(T.tensor([k2, observations['obs']]))
    k1 = P.sample('k1', PD.Normal(_k1_params[0], TNF.softplus(_k1_params[1])))
    _z2_params = _nn_model_z2(T.tensor([k1]))
    z2 = P.sample('z2', PD.Normal(_z2_params[0], TNF.softplus(_z2_params[1])))
    _y4_params = _nn_model_y4(T.tensor([z2]))
    y4 = P.sample('y4', PD.Normal(_y4_params[0], TNF.softplus(_y4_params[1])))
    _x8_params = _nn_model_x8(T.tensor([y4]))
    x8 = P.sample('x8', PD.Normal(_x8_params[0], TNF.softplus(_x8_params[1])))
    _x7_params = _nn_model_x7(T.tensor([y4, x8]))
    x7 = P.sample('x7', PD.Normal(_x7_params[0], TNF.softplus(_x7_params[1])))
    _y3_params = _nn_model_y3(T.tensor([y4, z2]))
    y3 = P.sample('y3', PD.Normal(_y3_params[0], TNF.softplus(_y3_params[1])))
    _x6_params = _nn_model_x6(T.tensor([y3]))
    x6 = P.sample('x6', PD.Normal(_x6_params[0], TNF.softplus(_x6_params[1])))
    _x5_params = _nn_model_x5(T.tensor([y3, x6]))
    x5 = P.sample('x5', PD.Normal(_x5_params[0], TNF.softplus(_x5_params[1])))
    _z1_params = _nn_model_z1(T.tensor([k1, z2]))
    z1 = P.sample('z1', PD.Normal(_z1_params[0], TNF.softplus(_z1_params[1])))
    _y2_params = _nn_model_y2(T.tensor([z1]))
    y2 = P.sample('y2', PD.Normal(_y2_params[0], TNF.softplus(_y2_params[1])))
    _x4_params = _nn_model_x4(T.tensor([y2]))
    x4 = P.sample('x4', PD.Normal(_x4_params[0], TNF.softplus(_x4_params[1])))
    _x3_params = _nn_model_x3(T.tensor([y2, x4]))
    x3 = P.sample('x3', PD.Normal(_x3_params[0], TNF.softplus(_x3_params[1])))
    _y1_params = _nn_model_y1(T.tensor([z1, y2]))
    y1 = P.sample('y1', PD.Normal(_y1_params[0], TNF.softplus(_y1_params[1])))
    _x2_params = _nn_model_x2(T.tensor([y1]))
    x2 = P.sample('x2', PD.Normal(_x2_params[0], TNF.softplus(_x2_params[1])))
    _x1_params = _nn_model_x1(T.tensor([y1, x2]))
    x1 = P.sample('x1', PD.Normal(_x1_params[0], TNF.softplus(_x1_params[1])))
    return T.tensor(observations['obs'])
