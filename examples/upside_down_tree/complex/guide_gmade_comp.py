import torch as T
import torch.distributions as TD
import torch.distributions.constraints as TDC
import torch.nn as TN
import torch.nn.functional as TNF
import pyro as P
import pyro.infer as PI
import pyro.optim as PO
import pyro.distributions as PD
import pyro.nn as PN
import igg
from gmade import GMADE
gmade_0_gmade_input_dim_dict = {'observations': 1}
gmade_0_gmade_dependency_dict = {'k2': ['observations'], 'z4': ['k2'], 'y8':
    ['z4'], 'x16': ['y8'], 'x15': ['y8', 'x16'], 'y7': ['z4', 'y8'], 'x14':
    ['y7'], 'x13': ['x14', 'y7'], 'z3': ['k2', 'z4'], 'y6': ['z3'], 'x12':
    ['y6'], 'x11': ['x12', 'y6'], 'y5': ['z3', 'y6'], 'x10': ['y5'], 'x9':
    ['x10', 'y5'], 'k1': ['k2', 'observations'], 'z2': ['k1'], 'y4': ['z2'],
    'x8': ['y4'], 'x7': ['y4', 'x8'], 'y3': ['y4', 'z2'], 'x6': ['y3'],
    'x5': ['y3', 'x6'], 'z1': ['k1', 'z2'], 'y2': ['z1'], 'x4': ['y2'],
    'x3': ['x4', 'y2'], 'y1': ['z1', 'y2'], 'x2': ['y1'], 'x1': ['x2', 'y1']}
gmade_0_gmade_var_dim_dict = {'k2': 1, 'z4': 1, 'y8': 1, 'x16': 1, 'x15': 1,
    'y7': 1, 'x14': 1, 'x13': 1, 'z3': 1, 'y6': 1, 'x12': 1, 'x11': 1, 'y5':
    1, 'x10': 1, 'x9': 1, 'k1': 1, 'z2': 1, 'y4': 1, 'x8': 1, 'x7': 1, 'y3':
    1, 'x6': 1, 'x5': 1, 'z1': 1, 'y2': 1, 'x4': 1, 'x3': 1, 'y1': 1, 'x2':
    1, 'x1': 1}
gmade_0_gmade_dist_type_dict = {'k2': 'norm', 'z4': 'norm', 'y8': 'norm',
    'x16': 'norm', 'x15': 'norm', 'y7': 'norm', 'x14': 'norm', 'x13':
    'norm', 'z3': 'norm', 'y6': 'norm', 'x12': 'norm', 'x11': 'norm', 'y5':
    'norm', 'x10': 'norm', 'x9': 'norm', 'k1': 'norm', 'z2': 'norm', 'y4':
    'norm', 'x8': 'norm', 'x7': 'norm', 'y3': 'norm', 'x6': 'norm', 'x5':
    'norm', 'z1': 'norm', 'y2': 'norm', 'x4': 'norm', 'x3': 'norm', 'y1':
    'norm', 'x2': 'norm', 'x1': 'norm'}
gmade_0_gmade_ = GMADE(gmade_0_gmade_input_dim_dict,
    gmade_0_gmade_dependency_dict, gmade_0_gmade_var_dim_dict,
    gmade_0_gmade_dist_type_dict, hidden_sizes=5, hidden_layers=2)


def model(observations={"obs" : 0}):
    P.module('gmade_0_gmade_', gmade_0_gmade_)
    observations = observations["obs"]
    gmade_0_input_dict = {'observations': observations}
    gmade_0_output_dict = gmade_0_gmade_(gmade_0_input_dict, {'k2': 'k2',
        'z4': 'z4', 'y8': 'y8', 'x16': 'x16', 'x15': 'x15', 'y7': 'y7',
        'x14': 'x14', 'x13': 'x13', 'z3': 'z3', 'y6': 'y6', 'x12': 'x12',
        'x11': 'x11', 'y5': 'y5', 'x10': 'x10', 'x9': 'x9', 'k1': 'k1',
        'z2': 'z2', 'y4': 'y4', 'x8': 'x8', 'x7': 'x7', 'y3': 'y3', 'x6':
        'x6', 'x5': 'x5', 'z1': 'z1', 'y2': 'y2', 'x4': 'x4', 'x3': 'x3',
        'y1': 'y1', 'x2': 'x2', 'x1': 'x1'})
    k2 = gmade_0_output_dict['k2']
    z4 = gmade_0_output_dict['z4']
    y8 = gmade_0_output_dict['y8']
    x16 = gmade_0_output_dict['x16']
    x15 = gmade_0_output_dict['x15']
    y7 = gmade_0_output_dict['y7']
    x14 = gmade_0_output_dict['x14']
    x13 = gmade_0_output_dict['x13']
    z3 = gmade_0_output_dict['z3']
    y6 = gmade_0_output_dict['y6']
    x12 = gmade_0_output_dict['x12']
    x11 = gmade_0_output_dict['x11']
    y5 = gmade_0_output_dict['y5']
    x10 = gmade_0_output_dict['x10']
    x9 = gmade_0_output_dict['x9']
    k1 = gmade_0_output_dict['k1']
    z2 = gmade_0_output_dict['z2']
    y4 = gmade_0_output_dict['y4']
    x8 = gmade_0_output_dict['x8']
    x7 = gmade_0_output_dict['x7']
    y3 = gmade_0_output_dict['y3']
    x6 = gmade_0_output_dict['x6']
    x5 = gmade_0_output_dict['x5']
    z1 = gmade_0_output_dict['z1']
    y2 = gmade_0_output_dict['y2']
    x4 = gmade_0_output_dict['x4']
    x3 = gmade_0_output_dict['x3']
    y1 = gmade_0_output_dict['y1']
    x2 = gmade_0_output_dict['x2']
    x1 = gmade_0_output_dict['x1']
    return T.tensor(observations)
