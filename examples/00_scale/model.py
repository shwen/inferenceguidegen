import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model(obs = 0.0):
    weight = P.sample("weight", PD.Normal(5.0, 1.0))
    return P.sample("measurement", PD.Normal(weight, 0.75), obs=T.tensor(obs))



