import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model_1(obs = 0.0):
    """
    What is the sorting order used in igg?
    
    xi variables are "left ladder"
    yi variables are "right ladder"
    w,z are the top/bottom of ladder
    """
    w = P.sample("w", PD.Normal(3.0,0.1))
    x1 = P.sample("x1", PD.Normal(w + 1, 0.1))
    x2 = P.sample("x2", PD.Normal(x1 + 1, 0.1))
    x3 = P.sample("x3", PD.Normal(x2 + 1, 0.1))
    x4 = P.sample("x4", PD.Normal(x3 + 1, 0.1))
    y1 = P.sample("y1", PD.Normal(w - 1, 0.1))
    y2 = P.sample("y2", PD.Normal(y1 - 1, 0.1))
    y3 = P.sample("y3", PD.Normal(y2 - 1, 0.1))
    y4 = P.sample("y4", PD.Normal(y3 - 1, 0.1))
    z = P.sample("z", PD.Normal(x4 + 10 * y4, 0.1))
    return P.sample("obs", PD.Normal(z, 0.01), obs=T.tensor(obs))


def model_2(obs = 0.0):
    """
    Same thing, but with x,y interleaving.
    """
    w = P.sample("w", PD.Normal(3.0,0.1))
    x1 = P.sample("x1", PD.Normal(w + 1, 0.1))
    y1 = P.sample("y1", PD.Normal(w - 1, 0.1))
    x2 = P.sample("x2", PD.Normal(x1 + 1, 0.1))
    y2 = P.sample("y2", PD.Normal(y1 - 1, 0.1))
    x3 = P.sample("x3", PD.Normal(x2 + 1, 0.1))
    y3 = P.sample("y3", PD.Normal(y2 - 1, 0.1))
    x4 = P.sample("x4", PD.Normal(x3 + 1, 0.1))
    y4 = P.sample("y4", PD.Normal(y3 - 1, 0.1))
    z = P.sample("z", PD.Normal(x4 + 10 * y4, 0.1))
    return P.sample("obs", PD.Normal(z, 0.01), obs=T.tensor(obs))