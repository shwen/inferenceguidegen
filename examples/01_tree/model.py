import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model(obs = 0.0):
    """
    Note that this model is inherently hard to learn ... It it just an illustration of v-shaped trails.
    """
    x11 = P.sample("x11", PD.Normal(3.0,0.1))
    y12 = P.sample("x12", PD.Normal(3.0,0.1)) + 2
    x21 = P.sample("x21", PD.Normal(3.0,0.1))
    y22 = P.sample("x22", PD.Normal(3.0,0.1)) - 2
    x1 = P.sample("x1", PD.Normal(x11 + y12, 0.1))
    x2 = P.sample("x2", PD.Normal(x21 + y22, 0.1))
    return P.sample("obs", PD.Normal(x1 + x2, 0.1), obs=T.tensor(obs))


