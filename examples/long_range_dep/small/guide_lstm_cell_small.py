import torch as T
import torch.distributions as TD
import torch.distributions.constraints as TDC
import torch.nn as TN
import torch.nn.functional as TNF
import pyro as P
import pyro.infer as PI
import pyro.optim as PO
import pyro.distributions as PD
import pyro.nn as PN
import igg
from gmade import GMADE

hidden_size = 8
num_layer = 1
lstm = TN.LSTMCell(2, hidden_size)
h_0 = TN.Parameter(T.zeros(1, hidden_size))
c_0 = TN.Parameter(T.zeros(1, hidden_size))

_nn_model_x = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](hidden_size, 2))
_nn_model_y = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](hidden_size, 2))
_nn_model_sub_l__i = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](hidden_size, 2))

def construct_input(obs, x=T.tensor(0)):
    x = x.repeat(1, 1)
    input = obs.repeat(1, 1)
    return T.cat((input, x), 1)
def model(M, observations=0.0):
    P.module('lstm', lstm)
    P.module('_nn_model_x', _nn_model_x)
    P.module('_nn_model_y', _nn_model_y)
    P.module('_nn_model_sub_l__i', _nn_model_sub_l__i)
    observations = T.tensor([observations['obs']])
    
    input_x = construct_input(observations)

    h_x,c_x = lstm(input_x, (h_0, c_0))

    _x_params = _nn_model_x(h_x[0])
    x = P.sample('x', PD.Normal(_x_params[0], TNF.softplus(_x_params[1])))

    input = construct_input(observations, x)
    h = h_x
    c = c_x

    for i in range(M):
        h_l,c_l = lstm(input, (h, c))
        l_i_params = _nn_model_sub_l__i(h_x[0])
        l_i = P.sample(f'l_{i}', PD.Normal(l_i_params[0], TNF.softplus(l_i_params[1])))
        input = construct_input(observations, l_i)
        h = h_l
        c = c_l
    
    h_y,c_y = lstm(input, (h, c))
    _y_params = _nn_model_y(h_y[0])
    y = P.sample('y', PD.Normal(_y_params[0], TNF.softplus(_y_params[1])))
    return x, y
