import torch as T
import torch.distributions as TD
import torch.distributions.constraints as TDC
import torch.nn as TN
import torch.nn.functional as TNF
import pyro as P
import pyro.infer as PI
import pyro.optim as PO
import pyro.distributions as PD
import pyro.nn as PN
import igg
from gmade import GMADE
_nn_model_sub_l__i = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](
    8, 8), PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn__site0 = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](9, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8))
_nn_model_y = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](1, 8),
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_model_x = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear](2, 8), 
    PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 2))
_nn_aggregate_result = PN.PyroModule[TN.Sequential](PN.PyroModule[TN.Linear
    ](3, 8), PN.PyroModule[TN.ReLU](), PN.PyroModule[TN.Linear](8, 8))


def model_sub(_hidden, i, M):
    P.module('_nn_model_sub_l__i', _nn_model_sub_l__i)
    P.module('_nn__site0', _nn__site0)
    if i < M:
        _l__i_params = _nn_model_sub_l__i(_hidden)
        l__i = P.sample(f'l_{i}', PD.Normal(_l__i_params[0], TNF.softplus(
            _l__i_params[1])))
        _site0 = model_sub(_nn__site0(T.cat([_hidden, T.tensor([l__i])])), 
            i + 1, M)
        return T.tanh(l__i) / M + _site0
    return 0


def model(M, observations=0.0):
    P.module('_nn_model_y', _nn_model_y)
    P.module('_nn_model_x', _nn_model_x)
    P.module('_nn_aggregate_result', _nn_aggregate_result)
    observations = T.tensor([observations['obs']])
    _y_params = _nn_model_y(observations)
    y = P.sample('y', PD.Normal(_y_params[0], TNF.softplus(_y_params[1])))
    _x_params = _nn_model_x(T.tensor([y, observations]))
    x = P.sample('x', PD.Normal(_x_params[0], TNF.softplus(_x_params[1])))
    aggregate_result = model_sub(_nn_aggregate_result(T.tensor([x, y, observations])
       ), 0, M)
    return x, y
