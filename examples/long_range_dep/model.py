import pyro as P
import pyro.distributions as PD
import torch as T
import numpy as np
import igg

def model_sub(i, M):
    if i < M: 
        return T.tanh(P.sample(f"l_{i}", PD.Normal(0, 10))) / M + model_sub(i + 1, M)
    return 0

def model(M, observations={'obs':0.0}):
    x = P.sample("x", PD.Normal(0, 10))
    aggregate_result = model_sub(0, M)
    y = P.sample("y", PD.Normal(0, 10))
    return P.sample("obs", PD.Normal(x + y + aggregate_result, 0.1), obs=T.tensor(observations['obs']))
