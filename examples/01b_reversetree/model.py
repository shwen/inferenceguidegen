import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model(obs={'z11': 0.0, 'z12': 0.0, 'z21': 0.0, 'z22': 0.0}):
    x = P.sample("x", PD.Normal(0.0, 1.0))
    x1 = P.sample("x1", PD.Normal(x - 1, 0.5))
    x2 = P.sample("x2", PD.Normal(x + 1, 0.5))
    P.sample("y11", PD.Normal(x1 - 1, 0.1), obs=obs['z11'])
    P.sample("y12", PD.Normal(x1 + 1, 0.1), obs=obs['z12'])
    P.sample("y21", PD.Normal(x2 - 1, 0.1), obs=obs['z21'])
    P.sample("y22", PD.Normal(x2 + 1, 0.1), obs=obs['z22'])


