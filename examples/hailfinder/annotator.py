# The only purpose of this is to generate the directives used in the guide.

import sys

fileName = sys.argv[1]
outName = sys.argv[2]

shapes = {
    'N0_7muVerMo': 4,
    'SubjVertMo': 4,
    'QGVertMotion': 4,
    'CombVerMo': 4,
    'AreaMeso_ALS': 4,
    'SatContMoist': 4,
    'RaoContMoist': 4,
    'CombMoisture': 4,
    'AreaMoDryAir': 4,
    'VISCloudCov': 3,
    'IRCloudCover': 3,
    'CombClouds': 3,
    'CldShadeOth': 3,
    'AMInstabMt': 3,
    'InsInMt': 3,
    'WndHodograph': 4,
    'OutflowFrMt': 3,
    'MorningBound': 3,
    'Boundaries': 3,
    'CldShadeConv': 3,
    'CompPlFcst': 3,
    'CapChange': 3,
    'LoLevMoistAd': 4,
    'InsChange': 3,
    'MountainFcst': 3,
    'Date': 6,
    'Scenario': 11,
    'ScenRelAMCIN': 2,
    'MorningCIN': 4,
    'AMCINInScen': 3,
    'CapInScen': 3,
    'ScenRelAMIns': 6,
    'LIfr12ZDENSd': 4,
    'AMDewptCalPl': 3,
    'AMInsWliScen': 3,
    'InsSclInScen': 3,
    'ScenRel3_4': 5,
    'LatestCIN': 4,
    'LLIW': 4,
    'CurPropConv': 4,
    'ScnRelPlFcst': 11,
    'PlainsFcst': 3,
    'N34StarFcst': 3,
    'R5Fcst': 3,
    'Dewpoints': 7,
    'LowLLapse': 4,
    'MeanRH': 3,
    'MidLLapse': 3,
    'MvmtFeatures': 4,
    'RHRatio': 3,
    'SfcWndShfDis': 7,
    'SynForcng': 5,
    'TempDis': 4,
    'WindAloft': 4,
    'WindFieldMt': 2,
    'WindFieldPln': 6,
}
obsKeys = [
    'R5Fcst',
    'Dewpoints',
    'LowLLapse',
    'MeanRH',
    'MidLLapse',
    'MvmtFeatures',
    'RHRatio',
    'SfcWndShfDis',
    'SynForcng',
    'TempDis',
    'WindAloft',
    'WindFieldMt',
    'WindFieldPln',
]

# The structure of a line is (for example)
#
# LowLLapse = P.sample("LowLLapse", PD.Categorical(logits=T.tensor(LowLLapse_get_params(Scenario))), obs=T.tensor(observations["LowLLapse"]))
#
# so we separate this along "logits=" and "obs="
def transform_line(l):
    x1, x2 = l.split('logits=')
    if 'obs=' in x2:
        x2, x3 = x2.split('obs=')
        hasObs = True
        x2 = x2[:-3] # Remove the "), "
    else:
        hasObs = False
        x2 = x2[:-2] # Remove the "))", one from P.sample, one from P.categorical
    name,_ = x1.split(' = ')

    assert name in shapes, f"{name} is not in shapes"

    shape = f"({shapes[name]},)"

    nameParams = f"{name}_logit"
    QUOTE = '"'

    # The third line is the call to the sample statement
    lineSampleArgs = [
        f"{QUOTE}{name}{QUOTE}",
        f"PD.Categorical({nameParams})",
    ]
    if hasObs:
        lineSampleArgs.append(f"obs=T.tensor(observations[{QUOTE}{name}{QUOTE}])")
    lineSample = f"{name} = P.sample(" + ", ".join(lineSampleArgs) + ")"


    return [
        f"{QUOTE}%{nameParams} SHAPE {shape};EKIND DETERMINISTIC;%{QUOTE}",
        f"{nameParams} = {x2}",
        lineSample,
    ]


    return name + " " + x2 + (" obs" if hasObs else " ")

def transform_signature(l):
    front,_ = l.split('(')
    # Build the observation dictionary
    dictString = [
        f"'{key}': 1"
        for key in obsKeys
    ]
    argObs = "observations={" + ", ".join(dictString) + "}"
    return f"{front}({argObs}):"

with open(outName, 'w') as outFile:
    with open(fileName) as file:
        lines = [line.rstrip() for line in file]
        for l in lines:
            if l.startswith('def model'):
                outFile.write(transform_signature(l) + '\n')
            elif l.startswith('    '):
                ls = transform_line(l[4:])
                for l2 in ls:
                    outFile.write('    ' + l2 + '\n')
            else:
                outFile.write(l + '\n')
