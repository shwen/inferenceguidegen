# Hailfinder

This is a real world example of a Bayes net.

To generate the `model.py` file, use
```fish
python3 annotator.py hailfinder.py model.py
```
