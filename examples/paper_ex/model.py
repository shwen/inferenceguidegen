import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model_sub(k, suffix):
    #u = P.sample(f"u{suffix}", PD.Uniform(-2, 2))
    u = P.sample(f"u{suffix}", PD.Normal(0, 2))
    if u > k:
        v = P.sample(f"v{suffix}", PD.Normal(0.,1.))
        return v
    else:
        "%lhs SHAPE ();%"
        lhs = model_sub(k, suffix + "_l")
        "%rhs SHAPE ();%"
        rhs = model_sub(k, suffix + "_r")
        return lhs + rhs

def embed(x):
    return x

def model(obs = 0.0):
    #k = P.sample("k", PD.Beta(2, 3))
    k = P.sample("k", PD.Normal(0., 1.))
    s = embed(model_sub(k, ""))
    return P.sample("obs", PD.Normal(s, 1e-6), obs=T.tensor(obs))
