import pyro as P
import pyro.distributions as PD
import torch as T

def get_dist(x):
    if x == 0:
        return (0.1,0.2,0.3,0.4)
    else:
        return (0.4,0.3,0.2,0.1)
    
    
def model(obs=1):
    q = P.sample("q", PD.Bernoulli(0.5))
    "%d SHAPE (4,);EKIND DETERMINISTIC;%"
    d = T.tensor(get_dist(q))
    y = P.sample("y", PD.Categorical(d))
    z = P.sample("z", PD.Normal(y, 0.1), obs=obs)