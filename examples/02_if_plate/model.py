import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model(i, obs = 0.0):
    q = P.sample(f"q_{i}", PD.Bernoulli(0.5))
    if q > 0:
        y = P.sample(f"y_{i}", PD.Normal(5.0,1.0))
        return P.sample(f"obs_{i}", PD.Normal(y, 0.1), obs=T.tensor(obs))
    else:
        return P.sample(f"obs_{i}", PD.Normal(-5.0, 0.1), obs=T.tensor(obs))

