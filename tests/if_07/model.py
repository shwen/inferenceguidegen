import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model(obs = 0.0):
    q = P.sample("q", PD.Normal(0.0, 1.0))
    p = P.sample("p", PD.Normal(0.0, 1.0))
    if q > 0.5:
        return P.sample("obs", PD.Normal(p, 0.1), obs=T.tensor(obs))
    else:
        return P.sample("obs", PD.Normal(-5.0, 0.1), obs=T.tensor(obs))