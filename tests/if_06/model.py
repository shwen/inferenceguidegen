import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model(obs1 = 0.0, obs2 = 0.0):
    a = P.sample("a", PD.Normal(5.0, 1.0))
    b = P.sample("b", PD.Normal(-5.0, 1.0))
    q = P.sample("q", PD.Normal(0.0, 1.0))
    if 0.5 < q:
        return P.sample("obs", PD.Normal(a, 0.1), obs=T.tensor(obs1))
    else:
        return P.sample("obs", PD.Normal(b, 0.1), obs=T.tensor(obs2))