import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model(obs = 0.0):
    p = P.sample("p", PD.Normal(0.0, 1.0))
    q = P.sample("q", PD.Normal(0.0, 1.0))
    if p > 0.5:
        r1 = P.sample("r1", PD.Normal(0.0, 1.0))
        w1 = r1 + q
        P.sample("obs1", PD.Normal(w1, 0.1), obs=T.tensor(obs))
    if p < 1.5:
        r2 = P.sample("r2", PD.Normal(0.0, 1.0))
        w2 = r2 + q
        P.sample("obs2", PD.Normal(w2, 0.1), obs=T.tensor(obs))