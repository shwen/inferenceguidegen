import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model(obs = 0.0):
    r = P.sample("r", PD.Normal(10.0, 1.0))
    q = P.sample("q", PD.Normal(r, 1))
    if q > 0.5:
        y = P.sample("y", PD.Normal(5.0, 1.0))
    else:
        y = P.sample("y", PD.Normal(-5.0, 1.0))
    z = y + r
    return P.sample("obs", PD.Normal(z, 0.1), obs=T.tensor(obs))