import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model(obs = 0.0):
    x = P.sample("x", PD.Normal(10.0, 1.0))
    y = P.sample("y", PD.Normal(-10.0, 1.0))
    w = x + y
    z = P.sample("z", PD.Normal(w, 1.0))
    return P.sample("obs", PD.Normal(x, 0.1), obs=T.tensor(obs))