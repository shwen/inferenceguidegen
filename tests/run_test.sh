TEST=$1 # name of the test case

if [[ $# -eq 0 ]] ; then
    echo 'Please provide the name of a test case...'
    exit 1
fi

BASEDIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)"

python3 $BASEDIR/../generate.py $BASEDIR/$TEST/model.py --output $BASEDIR/$TEST
