import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model(obs = 0.0):
    a = P.sample("a", PD.Normal(5.0, 1.0))
    b = P.sample("b", PD.Normal(-5.0, 1.0))
    q = P.sample("q", PD.Normal(0.0, 1.0))
    if 0.5 < q:
        y = P.sample("y", PD.Normal(a, 1.0))
    else:
        y = P.sample("y", PD.Normal(b, 1.0))
    return P.sample("obs", PD.Normal(y, 0.1), obs=T.tensor(obs))