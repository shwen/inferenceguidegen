import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model(obs = 0.0):
    r = P.sample("r", PD.Normal(0.0, 1.0))
    q = P.sample("q", PD.Normal(r, 1.0))
    p = q > 0.5
    if p:
        return P.sample("obs", PD.Normal(-5.0, 0.1), obs=T.tensor(obs))
    else:
        y = P.sample("y", PD.Normal(5.0, 1.0))
        return P.sample("obs", PD.Normal(y, 0.1), obs=T.tensor(obs))