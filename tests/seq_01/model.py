import pyro as P
import pyro.distributions as PD
import torch as T
import igg

def model(obs = 0.0):
    p = P.sample("p", PD.Normal(10.0, 1.0))
    q = P.sample("q", PD.Normal(-10.0, 1.0))
    r = p + q
    return P.sample("obs", PD.Normal(r, 0.1), obs=T.tensor(obs))