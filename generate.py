import os, sys, shutil, itertools



from pathlib import Path
import matplotlib.pyplot as pyplot
import ast, astor

import igg
import igg.code_generation

def plot_graph(ag: igg.ast_graph.AstGraph, path: Path):
    f = path / 'ast_graph.png'
    if f.exists():
        os.remove(str(f))
    pyplot.figure(1, figsize=(20,15))
    ag.plot_graph()
    pyplot.savefig(f)
    pyplot.clf()
def plot_call_graph(ag: igg.ast_graph.AstGraph, path: Path):
    f = path / 'call_graph.png'
    if f.exists():
        os.remove(str(f))
    pyplot.figure(1, figsize=(20,15))
    ag.plot_call_graph()
    pyplot.savefig(f)
    pyplot.clf()

ANNOTATION_DICT = {
    '@': 'entry',
    '~': 'return',
}

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Inference Graph Generator')
    parser.add_argument("model", type=str,
                        help="Input model.py file")
    parser.add_argument("--output", type=str,
                        help="Output directory", default="")
    parser.add_argument("--graph_only", dest="graph_only", action='store_true',
                        help="Generate graph only")
    parser.add_argument("--gmade", dest="use_gmade", action='store_true',
                        help="Use gmade to replace fully connected NNs")
    parser.add_argument('--verbose', '-v', action='count', default=0)
    parser.add_argument('--n_hidden_layers', type=int,
                        help="number of hidden layers of generated neural networks", default=2)
    parser.add_argument('--n_hidden_neurons', type=int,
                        help="number of hidden neurons of generated neural networks", default=16)
    parser.add_argument('--activation_fn', type=str,
                        help="activation function in generated neural networks", default="relu")
    args = parser.parse_args()

    file_in = Path(args.model)
    if args.output == "":
        args.output = file_in.parent
    dir_out = Path(args.output)

    # Input

    dir_out.mkdir(parents=True, exist_ok=True)
    if file_in != dir_out / 'model.py':
        shutil.copy(file_in, dir_out / 'model.py')

    with open(file_in, 'rb') as f:
        tree = ast.parse(f.read(), filename=file_in.name)
    ag = igg.ast_graph.AstGraph()
    ag.process(tree, verbose=args.verbose)

    plot_graph(ag, dir_out)
    plot_call_graph(ag, dir_out)
    if not args.graph_only:
        gen = igg.code_generation.CodeGenerator(
            ag,
            args.use_gmade,
            args.verbose,
            args.n_hidden_layers,
            args.n_hidden_neurons,
            args.activation_fn
        )
        tree_guide = gen.generate_tree()
        with open(dir_out / 'guide.py', 'w') as f:
            f.write(astor.to_source(tree_guide))

