# Inference Graph Generator

## Installation

Install everything in `requirements.txt` using PIP.

## Usage

The main script which drives the IGG is `generate.py`. Use
```
python3 generate.py -h
```
to see the list of flags.

The usage is
```
python3 generate.py INPUT OUTPUT_DIRECTORY [-vvv]
```
where `-vvv` specifies a verbosity of level 3. An AST graph and a guide program
will be placed in the same directory as the output.

Each function in the `INPUT` file prefixed by `model` will be considered
stochastic.

## Examples

In `examples`, a few example programs are given. Execute

```
python3 generate.py examples/00_scale.py --output examples/00_scale -vvv
python3 generate.py examples/06_for.py --output examples/06_for -vvv
python3 generate.py examples/07_hidden.py --output examples/07_hidden -vvv
```
To generate the guide programs. Then use `train.py` in each of the output
folders to visualize the training result.

## Testing

Use
```fish
python3 -m igg.expressions
python3 -m igg.ast_graph
python3 -m igg.code_generation
```
